﻿//-----------------------------------------------------------------------
// <copyright file="EngineManager.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-20, 13:16</last_modified>
// <summary>Headset management </summary>
//-----------------------------------------------------------------------
using System;
using System.IO;
using System.Threading;
using Emotiv;
using UnityEngine;

/// <summary>
/// Static class responsible for emotiv headset readings and operations.
/// </summary>
public static class EngineManager
{
    /// <summary>
    /// Internal variable to control if headset is connected or if its being used simulator.
    /// </summary>
    public static bool bIsEmoComposer = false;

    /// <summary>
    /// Internal variable to control debug log.
    /// </summary>
    public static bool bIsDebugEngine = false;

    /// <summary>
    /// Limits adjustment execution time, in miliseconds.
    /// </summary>
    private static int iLimitsAdjustmentExecutionTime = 20000;     // 20 seconds    

    /// <summary>
    /// To control if adjustment is in progress.
    /// </summary>
    private static bool bIsAdjustmentFinished = false;

    /// <summary>
    /// Lock variable of the bool adjustment in progress.
    /// </summary>
    private static System.Object _lock_bIsAdjustmentFinished = new System.Object();


    #region Public Thread shared variables
    /// <summary>
    /// Game engine UpAlpha readings array size.
    /// </summary>
    private const int iUpAlphaNumChannels = 2;

    /// <summary>
    /// Game engine DownAlpha readings array size.
    /// </summary>
    private const int iDownAlphaNumChannels = 6;

    /// <summary>
    /// Game engine LowBeta readings array size.
    /// </summary>
    private const int iDownLowBetaNumChannels = 3;

    /// <summary>
    /// Game engine HighBeta readings array size.
    /// </summary>
    private const int iDownHighBetaNumChannels = 3;

    /// <summary>
    /// Game engine UpAlpha readings array size.
    /// </summary>
    private static int iUpAlphaEngineCalculationsArraySize = iUpAlphaNumChannels * 10;

    /// <summary>
    /// Game engine UpAlpha readings array size.
    /// </summary>
    private static int iDownAlphaEngineCalculationsArraySize = iDownAlphaNumChannels * 10;

    /// <summary>
    /// Game engine UpAlpha readings array size.
    /// </summary>
    private static int iDownLowBetaEngineCalculationsArraySize = iDownLowBetaNumChannels * 10;

    /// <summary>
    /// Game engine UpAlpha readings array size.
    /// </summary>
    private static int iDownHighBetaEngineCalculationsArraySize = iDownHighBetaNumChannels * 10;

    /// <summary>
    /// Thread-shared Alpha to uptrain average readings.
    /// </summary>
    public static double[] dUpAlphaStoredReadings = new double[iUpAlphaEngineCalculationsArraySize];

    /// <summary>
    /// Thread-shared Alpha to downtrain average readings.
    /// </summary>
    public static double[] dDownAlphaStoredReadings = new double[iDownAlphaEngineCalculationsArraySize];

    /// <summary>
    /// Thread-shared low Beta to train average readings.
    /// </summary>
    public static double[] dDownLowBetaStoredReadings = new double[iDownLowBetaEngineCalculationsArraySize];

    /// <summary>
    /// Thread-shared high Beta to train average readings.
    /// </summary>
    public static double[] dDownHighBetaStoredReadings = new double[iDownHighBetaEngineCalculationsArraySize];

    /// <summary>
    /// Thread that does the actual readings, accessed from outside to control if active or not.
    /// </summary>
    public static Thread tReadingsThread;
    #endregion

    #region Timer Control variables
    /// <summary>
    /// Readings cycle control variable.
    /// </summary>
    public static bool bThrShouldStop;

    /// <summary>
    /// Readings cycle control variable.
    /// </summary>
    public static bool bThrAdjustmentShouldStop;

    /// <summary>
    /// Stopwatch execution time control, to avoid readings in a too short period. (same value readings).
    /// </summary>
    private static System.Diagnostics.Stopwatch cycleControl;

    /// <summary>
    /// Full thread duration, for log purposes.
    /// </summary>
    private static System.Diagnostics.Stopwatch fullTimer;

    /// <summary>
    /// The periodicy of the readings, in Miliseconds.
    /// </summary>
    private static int iReadingsCycle = 500;
    #endregion  

    #region Private Headset Readings Variables
    /// <summary>
    /// Headset engine connected user ID.
    /// </summary>
    private static int userID = -1;

    /// <summary>
    /// Headset engine.
    /// </summary>
    private static EmoEngine engine;

    /// <summary>
    /// If engine is running, to control thread start.
    /// </summary>
    private static bool bIsEngineStarted;

    /// <summary>
    /// Engine channel list, where Alpha is going to be uptrained.
    /// </summary>
    private static EdkDll.IEE_DataChannel_t[] channelListAlphaUptrain;

    /// <summary>
    /// Engine channel list, where Alpha is going to be downtrained.
    /// </summary>
    private static EdkDll.IEE_DataChannel_t[] channelListAlphaDowntrain;

    /// <summary>
    /// Engine channel list, where Beta is going to be uptrained or downtrained.
    /// </summary>
    private static EdkDll.IEE_DataChannel_t[] channelListBetaDowntrain; // Channels where beta is going to be downtrained

    #region dataChannelInfo
    ////public enum IEE_DataChannel_t
    ////{
    ////    IED_COUNTER = 0,        //!< Sample counter
    ////    IED_INTERPOLATED,       //!< Indicate if data is interpolated
    ////    IED_RAW_CQ,             //!< Raw contact quality value
    ////    IED_AF3,                //!< Channel AF3
    ////    IED_F7,                 //!< Channel F7
    ////    IED_F3,                 //!< Channel F3
    ////    IED_FC5,                //!< Channel FC5
    ////    IED_T7,                 //!< Channel T7
    ////    IED_P7,                 //!< Channel P7
    ////    IED_O1,                 //!< Channel O1 = Pz
    ////    IED_O2,                 //!< Channel O2
    ////    IED_P8,                 //!< Channel P8
    ////    IED_T8,                 //!< Channel T8
    ////    IED_FC6,                //!< Channel FC6
    ////    IED_F4,                 //!< Channel F4
    ////    IED_F8,                 //!< Channel F8
    ////    IED_AF4,                //!< Channel AF4
    ////    IED_GYROX,              //!< Gyroscope X-axis
    ////    IED_GYROY,              //!< Gyroscope Y-axis
    ////    IED_TIMESTAMP,          //!< System timestamp
    ////    IED_ES_TIMESTAMP,       //!< EmoState timestamp
    ////    IED_FUNC_ID,            //!< Reserved function id
    ////    IED_FUNC_VALUE,         //!< Reserved function value
    ////    IED_MARKER,             //!< Marker value from hardware
    ////    IED_SYNC_SIGNAL         //!< Synchronisation signal
    ////};
    #endregion

    /// <summary>
    /// Alpha frequency registered.
    /// </summary>
    private static double[] dAlpha;

    /// <summary>
    /// Low Beta frequency registered.
    /// </summary>
    private static double[] dLowBeta;

    /// <summary>
    /// High Beta frequency registered.
    /// </summary>
    private static double[] dHighBeta;

    /// <summary>
    /// Gamma frequency registered.
    /// </summary>
    private static double[] dGamma;

    /// <summary>
    /// Theta frequency registered.
    /// </summary>
    private static double[] dTheta;
    #endregion

    #region Private Limits Adjustment variables
    /// <summary>
    /// Adjustment array size.
    /// </summary>
    private static int iAdjustmentArraySize = 50;

    /// <summary>
    /// Alpha to uptrain adjustment readings.
    /// </summary>
    private static double[] dUpAlphaAdjustment = new double[iAdjustmentArraySize];

    /// <summary>
    /// Alpha to downtrain adjustment readings.
    /// </summary>
    private static double[] dDownAlphaAdjustment = new double[iAdjustmentArraySize];

    /// <summary>
    /// Low Beta to downtrain adjustment readings.
    /// </summary>
    private static double[] dDownLowBetaAdjustment = new double[iAdjustmentArraySize];

    /// <summary>
    /// High Beta to downtrain adjustment readings.
    /// </summary>
    private static double[] dDownHighBetaAdjustment = new double[iAdjustmentArraySize];
    #endregion

    #region Bandwidth StdDeviation
    /// <summary>
    /// Alpha uptrain standard deviation (uV).
    /// </summary>
    private static double dUpAlphaStdDeviation;

    /// <summary>
    /// Alpha downtrain standard deviation (uV).
    /// </summary>
    private static double dDownAlphaStdDeviation;

    /// <summary>
    /// Low beta downtrain standard deviation (uV).
    /// </summary>
    private static double dDownLowBetaStdDeviation;

    /// <summary>
    /// High beta downtrain standard deviation (uV).
    /// </summary>
    private static double dDownHighBetaStdDeviation;
    #endregion

    #region Private Headset calculation Variables
    /// <summary>
    /// Current uptrain Alpha array size (necessary for first iterations).
    /// </summary>
    private static int iUpAlphaSize;

    /// <summary>
    /// Current downtrain Alpha array size (necessary for first iterations).
    /// </summary>
    private static int iDownAlphaSize;

    /// <summary>
    /// Current LowBeta array size (necessary for first iterations).
    /// </summary>
    private static int iDownLowBetaSize;

    /// <summary>
    /// Current HighBeta array size (necessary for first iterations).
    /// </summary>
    private static int iDownHighBetaSize;

    /// <summary>
    /// Current uptrain Alpha adjustment array size (necessary for first iterations).
    /// </summary>
    private static int iUpAlphaAdjustmentSize;

    /// <summary>
    /// Current downtrain Alpha adjustment array size (necessary for first iterations).
    /// </summary>
    private static int iDownAlphaAdjustmentSize;

    /// <summary>
    /// Current LowBeta adjustment array size (necessary for first iterations).
    /// </summary>
    private static int iDownLowBetaAdjustmentSize;

    /// <summary>
    /// Current HighBeta adjustment array size (necessary for first iterations).
    /// </summary>
    private static int iDownHighBetaAdjustmentSize;
    #endregion

    #region Getters
    /// <summary>
    /// Gets current adjustment in progress state shared by threads.
    /// </summary>
    /// <returns>Is adjustment in progress bool.</returns>
    public static bool GetIsAdjustmentFinished()
    {
        // Lock the access to the variable and reads it
        lock (_lock_bIsAdjustmentFinished)
        {
            return bIsAdjustmentFinished;
        }
    }

    /// <summary>
    /// Gets uptrain Alpha adjustment standard deviation amplitude shared by threads.
    /// </summary>
    /// <returns>Calculated Alpha adjustment period standard deviation amplitude to uptrain.</returns>
    public static double GetUpAlphaStdDeviation()
    {
        // Lock the access to the variable and reads it
        lock (dUpAlphaAdjustment)
        {
            return CalculateStdDeviation(dUpAlphaAdjustment);
        }
    }

    /// <summary>
    /// Gets downtrain Alpha adjustment standard deviation amplitude shared by threads.
    /// </summary>
    /// <returns>Calculated Alpha adjustment period standard deviation amplitude to downtrain.</returns>
    public static double GetDownAlphaStdDeviation()
    {
        // Lock the access to the variable and reads it
        lock (dDownAlphaAdjustment)
        {
            return CalculateStdDeviation(dDownAlphaAdjustment);
        }
    }

    /// <summary>
    /// Gets downtrain low beta adjustment standard deviation amplitude shared by threads.
    /// </summary>
    /// <returns>Calculated low beta adjustment period standard deviation amplitude to downtrain.</returns>
    public static double GetDownLowBetaStdDeviation()
    {
        // Lock the access to the variable and reads it
        lock (dDownLowBetaAdjustment)
        {
            return CalculateStdDeviation(dDownLowBetaAdjustment);
        }
    }

    /// <summary>
    /// Gets downtrain high beta adjustment standard deviation amplitude shared by threads.
    /// </summary>
    /// <returns>Calculated high beta adjustment period standard deviation amplitude to downtrain.</returns>
    public static double GetDownHighBetaStdDeviation()
    {
        // Lock the access to the variable and reads it
        lock (dDownHighBetaAdjustment)
        {
            return CalculateStdDeviation(dDownHighBetaAdjustment);
        }
    }

    /// <summary>
    /// Gets uptrain Alpha adjustment average amplitude shared by threads.
    /// </summary>
    /// <returns>Calculated Alpha adjustment period average amplitude to uptrain.</returns>
    public static double GetUpAlphaAdjustmentAverage()
    {
        // Lock the access to the variable and reads it
        lock (dUpAlphaAdjustment)
        {
            return CalculateAverage(dUpAlphaAdjustment);
        }
    }

    /// <summary>
    /// Gets downtrain Alpha adjustment average amplitude shared by threads.
    /// </summary>
    /// <returns>Calculated Alpha adjustment period average amplitude to downtrain.</returns>
    public static double GetDownAlphaAdjustmentAverage()
    {
        // Lock the access to the variable and reads it
        lock (dDownAlphaAdjustment)
        {
            return CalculateAverage(dDownAlphaAdjustment);
        }
    }

    /// <summary>
    /// Gets downtrain LowBeta adjustment average amplitude shared by threads.
    /// </summary>
    /// <returns>Calculated LowBeta adjustment period average amplitude to downtrain.</returns>
    public static double GetDownLowBetaAdjustmentAverage()
    {
        // Lock the access to the variable and reads it
        lock (dDownLowBetaAdjustment)
        {
            return CalculateAverage(dDownLowBetaAdjustment);
        }
    }

    /// <summary>
    /// Gets downtrain HighBeta adjustment average amplitude shared by threads.
    /// </summary>
    /// <returns>Calculated HighBeta adjustment period average amplitude to downtrain.</returns>
    public static double GetDownHighBetaAdjustmentAverage()
    {
        // Lock the access to the variable and reads it
        lock (dDownHighBetaAdjustment)
        {
            return CalculateAverage(dDownHighBetaAdjustment);
        }
    }

    /// <summary>
    /// Get uptrain Alpha average variable shared by threads.
    /// </summary>
    /// <returns>Calculated Alpha average readings to uptrain.</returns>
    public static double GetUpAlphaAverage()
    {
        // Lock the access to the variable and reads it
        lock (dUpAlphaStoredReadings)
        {
            return CalculateAverage(dUpAlphaStoredReadings);
        }
    }

    /// <summary>
    /// Get downtrain Alpha average variable shared by threads.
    /// </summary>
    /// <returns>Calculated Alpha average readings to downtrain.</returns>
    public static double GetDownAlphaAverage()
    {
        // Lock the access to the variable and reads it
        lock (dDownAlphaStoredReadings)
        {
            return CalculateAverage(dDownAlphaStoredReadings);
        }
    }

    /// <summary>
    /// Get downtrain lowbeta average variable shared by threads.
    /// </summary>
    /// <returns>Calculated lowBeta average readings.</returns>
    public static double GetDownLowBetaAverage()
    {
        // Lock the access to the variable and reads it
        lock (dDownLowBetaStoredReadings)
        {
            return CalculateAverage(dDownLowBetaStoredReadings);
        }
    }

    /// <summary>
    /// Get downtrain highbeta average variable shared by threads.
    /// </summary>
    /// <returns>Calculated HighBeta average readings.</returns>
    public static double GetDownHighBetaAverage()
    {
        // Lock the access to the variable and reads it
        lock (dDownHighBetaStoredReadings)
        {
            return CalculateAverage(dDownHighBetaStoredReadings);
        }
    }

    /// <summary>
    /// Gets last uptrain alpha value registered. For debug.
    /// </summary>
    /// <returns>Most recent value registered.</returns>
    public static double GetLastUpAlpha()
    {
        // Lock the access to the variable and reads it
        lock (dUpAlphaStoredReadings)
        {
            return dAlpha[0];
        }
    }

    /// <summary>
    /// Gets last downtrain alpha value registered. For debug.
    /// </summary>
    /// <returns>Most recent value registered.</returns>
    public static double GetLastDownAlpha()
    {
        // Lock the access to the variable and reads it
        lock (dDownAlphaStoredReadings)
        {
            return dAlpha[0];
        }
    }

    /// <summary>
    /// Gets last downtrain lowbeta value registered. For debug.
    /// </summary>
    /// <returns>Most recent value registered.</returns>
    public static double GetLastDownLowBeta()
    {
        // Lock the access to the variable and writes it
        lock (dDownLowBetaStoredReadings)
        {
            return dLowBeta[0];
        }
    }

    /// <summary>
    /// Gets last downtrain highbeta value registered. For debug.
    /// </summary>
    /// <returns>Most recent value registered.</returns>
    public static double GetLastDownHighBeta()
    {
        // Lock the access to the variable and writes it
        lock (dDownHighBetaStoredReadings)
        {
            return dHighBeta[0];
        }
    }
    #endregion

    #region Setters
    /// <summary>
    /// Sets if adjustment is currently in progress.
    /// </summary>
    /// <param name="newAdjustmentFinishedState">New bool state of adjustment in progress.</param>
    public static void SetIsAdjustmentFinished(bool newAdjustmentFinishedState)
    {
        lock (_lock_bIsAdjustmentFinished)
        {
            bIsAdjustmentFinished = newAdjustmentFinishedState;
        }
    }

    /// <summary>
    /// Set Alpha variable to uptrain shared by threads.
    /// </summary>
    /// <param name="newUpAlpha"> New value to set for uptrain Alpha.</param>
    public static void SetUpAlpha(double[] newUpAlpha)
    {
        lock (dUpAlphaStoredReadings)
        {
            PushIntoArrayEnd(dUpAlphaStoredReadings, newUpAlpha[0], ref iUpAlphaSize, iUpAlphaEngineCalculationsArraySize);
        }
    }

    /// <summary>
    /// Set Alpha variable to downtrain shared by threads.
    /// </summary>
    /// <param name="newDownAlpha"> New value to set for downtrain Alpha.</param>
    public static void SetDownAlpha(double[] newDownAlpha)
    {
        lock (dDownAlphaStoredReadings)
        {
            PushIntoArrayEnd(dDownAlphaStoredReadings, newDownAlpha[0], ref iDownAlphaSize, iDownAlphaEngineCalculationsArraySize);
        }
    }

    /// <summary>
    /// Set low Beta variable shared by threads.
    /// </summary>
    /// <param name="newDownLowBeta"> New value to set for low Beta.</param>
    public static void SetDownLowBeta(double[] newDownLowBeta)
    {
        lock (dDownLowBetaStoredReadings)
        {
            PushIntoArrayEnd(dDownLowBetaStoredReadings, newDownLowBeta[0], ref iDownLowBetaSize, iDownLowBetaEngineCalculationsArraySize);
        }
    }

    /// <summary>
    /// Set high Beta variable shared by threads.
    /// </summary>
    /// <param name="newDownHighbeta"> New value to set for high Beta.</param>
    public static void SetDownHighBeta(double[] newDownHighbeta)
    {
        lock (dDownHighBetaStoredReadings)
        {
            PushIntoArrayEnd(dDownHighBetaStoredReadings, newDownHighbeta[0], ref iDownHighBetaSize, iDownHighBetaEngineCalculationsArraySize);
        }
    }

    /// <summary>
    /// Set Alpha variable to uptrain to adjust limits.
    /// </summary>
    /// <param name="newUpAlpha">New value to set for adjustment of uptrain Alpha limit.</param>
    public static void SetUpAlphaAdjustment(double[] newUpAlpha)
    {
        lock (dUpAlphaAdjustment)
        {
            PushIntoArrayEnd(dUpAlphaAdjustment, newUpAlpha[0], ref iUpAlphaAdjustmentSize, iAdjustmentArraySize);
        }
    }

    /// <summary>
    /// Set Alpha variable to downtrain to adjust limits.
    /// </summary>
    /// <param name="newDownAlpha">New value to set for adjustment of downtrain Alpha limit.</param>
    public static void SetDownAlphaAdjustment(double[] newDownAlpha)
    {
        lock (dDownAlphaAdjustment)
        {
            PushIntoArrayEnd(dDownAlphaAdjustment, newDownAlpha[0], ref iDownAlphaAdjustmentSize, iAdjustmentArraySize);
        }
    }

    /// <summary>
    /// Set low Beta variable to adjust limits.
    /// </summary>
    /// <param name="newDownLowBeta">New value to set for adjustment of low Beta limit.</param>
    public static void SetDownLowBetaAdjustment(double[] newDownLowBeta)
    {
        lock (dDownLowBetaAdjustment)
        {
            PushIntoArrayEnd(dDownLowBetaAdjustment, newDownLowBeta[0], ref iDownLowBetaAdjustmentSize, iAdjustmentArraySize);
        }
    }

    /// <summary>
    /// Set high Beta variable to adjust limits.
    /// </summary>
    /// <param name="newDownHighbeta">New value to set for adjustment of high Beta limit.</param>
    public static void SetDownHighBetaAdjustment(double[] newDownHighbeta)
    {
        lock (dDownHighBetaAdjustment)
        {
            PushIntoArrayEnd(dDownHighBetaAdjustment, newDownHighbeta[0], ref iDownHighBetaAdjustmentSize, iAdjustmentArraySize);
        }
    }
    #endregion

    #region Auxiliary Methods
    /// <summary>
    /// Shifts array position to the left, if necessary.
    /// </summary>
    /// <param name="arr">Array to be shifted.</param>
    /// <param name="newValueToAdd">New value to add to array.</param>
    /// <param name="currentArrSize">Current array size variable.</param>
    /// <param name="maxArraySize">Maximum array size variable, for control.</param>
    public static void PushIntoArrayEnd(double[] arr, double newValueToAdd, ref int currentArrSize, int maxArraySize)
    {
        int index = 0;

        if (currentArrSize < maxArraySize)
        {
            arr[currentArrSize] = newValueToAdd;
            currentArrSize++;
        }
        else
        {
            for (int i = 0; i < arr.Length - 1; i++)
            {
                arr[index] = arr[i + 1];
                index++;
            }

            arr[currentArrSize - 1] = newValueToAdd;
        }
    }

    /// <summary>
    /// Calculates average of an array.
    /// </summary>
    /// <param name="arr">Array to calculate the average.</param>
    /// <returns>Double average value.</returns>
    public static double CalculateAverage(double[] arr)
    {
        double toReturn = 0;       // Average result

        // Calculates average of current existing values
        for (int i = 0; i < arr.Length; i++)
        {
            toReturn += arr[i];
        }

        return toReturn / arr.Length;
    }

    /// <summary>
    /// Calculates standard deviation of an array and stores in the indicated variable.
    /// </summary>
    /// <param name="arr">Array to calculate the deviation.</param>
    public static double CalculateStdDeviation(double[] arr)
    {
        // Calculates average
        double mean = CalculateAverage(arr);
        double tempSum = 0;

        // Stores in a temp variable the sum of the distances
        for (int i = 0; i < arr.Length; i++)
        {
            tempSum +=Math.Pow(Math.Abs(arr[i] - mean), 2);
        }

        // Returns the result square root
        return Math.Sqrt(tempSum / arr.Length);
    }
    #endregion

    #region ValueSimulation
    /// <summary>
    /// Simulator: increase uptrain alpha.
    /// </summary>
    public static void IncreaseUpAlpha()
    {
        dAlpha[0] += 5;
        if(GetIsAdjustmentFinished())
        {
            SetUpAlpha(dAlpha);
        }
        else
        {
            SetUpAlphaAdjustment(dAlpha);
        }
        Debug.Log("increasing upalpha");
        
    }

    /// <summary>
    /// Simulator: decrease uptrain alpha.
    /// </summary>
    public static void DecreaseUpAlpha()
    {
        dAlpha[0] -= 5;
        if (dAlpha[0] < 0)
        {
            dAlpha[0] = 0;
        }

        if (GetIsAdjustmentFinished())
        {
            SetUpAlpha(dAlpha);
        }
        else
        {
            SetUpAlphaAdjustment(dAlpha);
        }

        Debug.Log("decreasing upalpha");

    }

    /// <summary>
    /// Simulator: increase downtrain alpha.
    /// </summary>
    public static void IncreaseDownAlpha()
    {
        dAlpha[0] += 5;

        if (GetIsAdjustmentFinished())
        {
            SetDownAlpha(dAlpha);
        }
        else
        {
            SetDownAlphaAdjustment(dAlpha);
        }

        Debug.Log("increasing downalpha");
    }

    /// <summary>
    /// Simulator: decrease downtrain alpha.
    /// </summary>
    public static void DecreaseDownAlpha()
    {
        dAlpha[0] -= 5;
        if (dAlpha[0] < 0)
        {
            dAlpha[0] = 0;
        }

        if(GetIsAdjustmentFinished())
        {
            SetDownAlpha(dAlpha);
        }
        else
        {
            SetDownAlphaAdjustment(dAlpha);
        }

        Debug.Log("decreasing downalpha");
    }

    /// <summary>
    /// Simulator: increase downtrain low and high beta.
    /// </summary>
    public static void IncreaseDownBeta()
    {
        dLowBeta[0] += 1;
        dHighBeta[0] += 1;

        if(GetIsAdjustmentFinished())
        {
            SetDownLowBeta(dLowBeta);
            SetDownHighBeta(dHighBeta);
        }
        else
        {
            SetDownLowBetaAdjustment(dLowBeta);
            SetDownHighBetaAdjustment(dHighBeta);
        }

        Debug.Log("increasing downbeta");
    }

    /// <summary>
    /// Simulator: decrease downtrain low and high beta.
    /// </summary>
    public static void DecreaseDownBeta()
    {
        dLowBeta[0] -= 1;
        dHighBeta[0] -= 1;
        if (dLowBeta[0] < 0)
        {
            dLowBeta[0] = 0;
        }

        if (dHighBeta[0] < 0)
        {
            dHighBeta[0] = 0;
        }

        if(GetIsAdjustmentFinished())
        {
            SetDownLowBeta(dLowBeta);
            SetDownHighBeta(dHighBeta);
        }
        else
        {
            SetDownLowBetaAdjustment(dLowBeta);
            SetDownHighBetaAdjustment(dHighBeta);
        }
        Debug.Log("decreasing downbeta");
    }
    #endregion

    /// <summary>
    /// Sets engine parameters and control the thread start.
    /// </summary>
    public static void InitializeHeadsetSettings()
    {
        bThrShouldStop = false;
        bThrAdjustmentShouldStop = false;
        bIsEngineStarted = false;

        GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::InitializeHeadsetSettings() - Getting headset settings ready...");

        if(bIsDebugEngine)
        {
            Debug.Log(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": Initializing EngineManager Settings...");
        }
        
        engine = EmoEngine.Instance;

        // Starts variables used to store the direct readings
        dAlpha = new double[1];
        dLowBeta = new double[1];
        dHighBeta = new double[1];
        dGamma = new double[1];
        dTheta = new double[1];

        // Resets array sizes
        iUpAlphaSize = 0;
        iDownAlphaSize = 0;
        iDownLowBetaSize = 0;
        iDownHighBetaSize = 0;
        iUpAlphaAdjustmentSize = 0;
        iDownAlphaAdjustmentSize = 0;
        iDownLowBetaAdjustmentSize = 0;
        iDownHighBetaAdjustmentSize = 0;

        // Sets control variable adjustment in progress to true.
        SetIsAdjustmentFinished(false);

        // Add event listeners
        engine.EmoEngineConnected += new EmoEngine.EmoEngineConnectedEventHandler(Engine_EmoEngineConnected);
        engine.EmoEngineDisconnected += new EmoEngine.EmoEngineDisconnectedEventHandler(Engine_EmoEngineDisconnected);
        engine.UserAdded += new EmoEngine.UserAddedEventHandler(Engine_UserAdded);
        engine.UserRemoved += new EmoEngine.UserRemovedEventHandler(Engine_UserRemoved);

        // Connect to headset/emocomposer
        try
        {
            // If is set to connect to emocomposer
            if (bIsEmoComposer)
            {
                GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::InitializeHeadsetReadings() - Going to connect to EmoComposer.");
                engine.RemoteConnect("127.0.0.1", 1726);
            }
            else
            {
                GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::InitializeHeadsetReadings() - Going to connect to Headset.");

                // Connects to headset
                engine.Connect();
            }

            bIsEngineStarted = true;
        }
        catch (Exception e)
        {
            GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::InitializeHeadsetReadings() - Failed to connect headset/composer. Thread not started: " + e.ToString());
            MenuUI.AddDialogToList(32);
        }
    }

    public static void StartReadingsProcedure()
    {
        if (bIsEngineStarted)
        {
            // Sets the channelList to the pretended channels
            // Uptrain Alpha Posterior
            channelListAlphaUptrain = new EdkDll.IEE_DataChannel_t[iUpAlphaNumChannels] { EdkDll.IEE_DataChannel_t.IED_P7, EdkDll.IEE_DataChannel_t.IED_P8 };

            // Downtrain Alpha Anterior
            channelListAlphaDowntrain = new EdkDll.IEE_DataChannel_t[iDownAlphaNumChannels] { EdkDll.IEE_DataChannel_t.IED_AF3, EdkDll.IEE_DataChannel_t.IED_AF4, EdkDll.IEE_DataChannel_t.IED_F3, EdkDll.IEE_DataChannel_t.IED_F4, EdkDll.IEE_DataChannel_t.IED_F7, EdkDll.IEE_DataChannel_t.IED_F8 };

            // Downtrain Beta in RH Anterior
            channelListBetaDowntrain = new EdkDll.IEE_DataChannel_t[iDownLowBetaNumChannels] { EdkDll.IEE_DataChannel_t.IED_AF4, EdkDll.IEE_DataChannel_t.IED_F4, EdkDll.IEE_DataChannel_t.IED_F8 };

            try
            {
                // Sets new thread
                tReadingsThread = new Thread(ThrLimitsAdjustments);

                // Debug.Log("going to call new thread");
                // Starts timer to control thread execution (for test purposes only)
                fullTimer = System.Diagnostics.Stopwatch.StartNew();

                // Starts thread
                tReadingsThread.Start();
            }
            catch (Exception e)
            {
                GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::StartReadingsProcedure() - Failed to create new thread: " + e.ToString());
                Debug.LogError(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::StartReadingsProcedure() - Failed to create new thread: " + e.ToString());
            }
        }
    }

    /// <summary>
    /// Threaded method. Makes the beginning of the game limits adjustments.
    /// </summary>
    public static void ThrLimitsAdjustments()
    {
        GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrLimitsAdjustments() - Starting thread...");

        if (bIsDebugEngine)
        {
            Debug.Log(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrLimitsAdjustments() - Starting thread...");
        }

        // Starts new stopwatch to control readings execution timing
        cycleControl = System.Diagnostics.Stopwatch.StartNew();

        // Adjustment execution control
        while (!bThrAdjustmentShouldStop)
        {
            // Readings is set to run 2 times per second. Verifies if enough time has elapsed
            if (cycleControl.ElapsedMilliseconds > iReadingsCycle)
            {
                //Debug.Log(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrLimitsAdjustments() - going to execute reading");
                try
                {
                    // Processes EmoEngine events until there is no more events or maximum processing time (set to 10 milliseconds) has elapsed
                    engine.ProcessEvents(10);
                    GameManager.rawBandPowersLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrLimitsAdjustments()");
                    GameManager.adjustmentPeriodLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrLimitsAdjustments()");
                    for (int i = 0; i < channelListAlphaUptrain.Length; i++)
                    {
                        // If is set to connect to headset
                        if (!bIsEmoComposer)
                        {
                            // Gets headset average bandpowers
                            engine.IEE_GetAverageBandPowers((uint)userID, channelListAlphaUptrain[i], dTheta, dAlpha, dLowBeta, dHighBeta, dGamma);

                            // Sets the value for limit adjustment
                            SetUpAlphaAdjustment(dAlpha);
                        }

                        // Writes to log files
                        GameManager.rawBandPowersLog.WriteLine("AlphaUp- Channel: " + channelListAlphaUptrain[i] + ", value: " + dAlpha[0]);
                        GameManager.adjustmentPeriodLog.WriteLine("AlphaUp- Channel: " + channelListAlphaUptrain[i] + ", value: " + dAlpha[0]);
                    }

                    for (int i = 0; i < channelListAlphaDowntrain.Length; i++)
                    {
                        // If is set to connect to headset
                        if (!bIsEmoComposer)
                        {
                            // Gets headset average bandpowers
                            engine.IEE_GetAverageBandPowers((uint)userID, channelListAlphaDowntrain[i], dTheta, dAlpha, dLowBeta, dHighBeta, dGamma);

                            //// Sets the value for limit adjustment
                            SetDownAlphaAdjustment(dAlpha);
                        }
                        // Writes to log files
                        GameManager.rawBandPowersLog.WriteLine("AlphaDown- Channel: " + channelListAlphaDowntrain[i] + ", value: " + dAlpha[0]);
                        GameManager.adjustmentPeriodLog.WriteLine("AlphaDown- Channel: " + channelListAlphaDowntrain[i] + ", value: " + dAlpha[0]);
                    }

                    for (int i = 0; i < channelListBetaDowntrain.Length; i++)
                    {
                        // If is set to connect to headset
                        if (!bIsEmoComposer)
                        {
                            // Gets headset average bandpowers
                            engine.IEE_GetAverageBandPowers((uint)userID, channelListBetaDowntrain[i], dTheta, dAlpha, dLowBeta, dHighBeta, dGamma);

                            // Sets the value into the shared array
                            SetDownLowBetaAdjustment(dLowBeta);

                            // Sets the value into the shared array
                            SetDownHighBetaAdjustment(dHighBeta);
                        }
                        // Writes to log files
                        GameManager.rawBandPowersLog.WriteLine("LowBeta- Channel: " + channelListBetaDowntrain[i] + ", value: " + dLowBeta[0]);
                        GameManager.rawBandPowersLog.WriteLine("HighBeta- Channel: " + channelListBetaDowntrain[i] + ", value: " + dHighBeta[0]);
                        GameManager.adjustmentPeriodLog.WriteLine("LowBeta- Channel: " + channelListBetaDowntrain[i] + ", value: " + dLowBeta[0]);
                        GameManager.adjustmentPeriodLog.WriteLine("HighBeta- Channel: " + channelListBetaDowntrain[i] + ", value: " + dHighBeta[0]);
                    }

                    GameManager.rawBandPowersLog.WriteLine("-------------------Finished adjustment cycle-------------------");
                    GameManager.adjustmentPeriodLog.WriteLine("-------------------Finished adjustment cycle-------------------");
                }
                catch (Exception e)
                {
                    GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrLimitsAdjustments() - Error getting bandpowers: " + e.ToString());
                    Debug.LogError(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrLimitsAdjustments() - Error getting bandpowers: " + e.ToString());
                }

                // Restarts the stopwatch to control the next reading
                cycleControl.Stop();
                cycleControl = System.Diagnostics.Stopwatch.StartNew();
            }

            // If fullTimer hits limit adjustment execution time stops and calls new method
            if (fullTimer.ElapsedMilliseconds >= iLimitsAdjustmentExecutionTime)
            {
                GameManager.rawBandPowersLog.WriteLine("-------------------Finished adjustment-------------------\n\n\n");
                GameManager.adjustmentPeriodLog.WriteLine("\n\n--------------------------------------------------------------");
                GameManager.adjustmentPeriodLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrLimitsAdjustments() - Finished adjustment. Resulting array:");

                // Logs resulting array into log file
                DebuggingTools.PrintArrayToLogFile(dUpAlphaAdjustment, GameManager.adjustmentPeriodLog);
                
                // Flags that the countdown is finished.
                bThrAdjustmentShouldStop = true;
            }
        }

        // If adjustment is finished, calls calculations method
        if (bThrAdjustmentShouldStop)
        {
            // Sets adjustment is finished control variable to true.
            SetIsAdjustmentFinished(true);

            GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrLimitsAdjustments() - Finished adjustment.");
            if (bIsDebugEngine)
            {
                Debug.Log(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrLimitsAdjustments() -  Finished Adjustment. Going to start runtime calculations.");
            }

            // Calls engine readings method.
            ThrEngineCalculations();
        }
    }

    /// <summary>
    /// Threaded method. Manages headset readings.
    /// </summary>
    public static void ThrEngineCalculations()
    {
        GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrEngineCalculations() -  Starting EngineCalculations...");
        if (bIsDebugEngine)
        {
            Debug.Log(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrEngineCalculations() -  Starting EngineCalculations...");
        }

        // Starts new stopwatch to control speed of readings execution
        cycleControl = System.Diagnostics.Stopwatch.StartNew();

        // Thread execution control
        while (!bThrShouldStop)
        {
            // Readings is set to run 2 times per second. Verifies if enough time has elapsed
            if (cycleControl.ElapsedMilliseconds > iReadingsCycle)
            {
                try
                {
                    // Processes EmoEngine events until there is no more events or maximum processing time (set to 10 milliseconds) has elapsed
                    engine.ProcessEvents(10);

                    GameManager.rawBandPowersLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrEngineCalculations()");
                    GameManager.averageBandPowersLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrEngineCalculations()");

                    for (int i = 0; i < channelListAlphaUptrain.Length; i++)
                    {
                        // If is set to connect to headset
                        if (!bIsEmoComposer)
                        {
                            // Gets headset average bandpowers
                            engine.IEE_GetAverageBandPowers((uint)userID, channelListAlphaUptrain[i], dTheta, dAlpha, dLowBeta, dHighBeta, dGamma);

                            //// Sets the value into the shared array
                            SetUpAlpha(dAlpha);
                        }
                        // Writes to log files
                        GameManager.rawBandPowersLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": AlphaUp- Channel: " + channelListAlphaUptrain[i] + ", value: " + dAlpha[0]);

                        GameManager.averageBandPowersLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": AlphaUp value: " + dAlpha[0] + " | Current average: " + GetUpAlphaAverage());
                    }

                    for (int i = 0; i < channelListAlphaDowntrain.Length; i++)
                    {
                        // If is set to connect to headset
                        if (!bIsEmoComposer)
                        {
                            // Gets headset average bandpowers
                            engine.IEE_GetAverageBandPowers((uint)userID, channelListAlphaDowntrain[i], dTheta, dAlpha, dLowBeta, dHighBeta, dGamma);

                            //// Sets the value into the shared array
                            SetDownAlpha(dAlpha);
                        }
                        
                        // Writes to log files
                        GameManager.rawBandPowersLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": AlphaDown- Channel: " + channelListAlphaDowntrain[i] + ", value: " + dAlpha[0]);
                        GameManager.averageBandPowersLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": AlphaDown value: " + dAlpha[0] + " | Current Average: " + GetDownAlphaAverage());
                    }

                    for (int i = 0; i < channelListBetaDowntrain.Length; i++)
                    {
                        // If is set to connect to headset
                        if (!bIsEmoComposer)
                        {
                            // Gets headset average bandpowers
                            engine.IEE_GetAverageBandPowers((uint)userID, channelListBetaDowntrain[i], dTheta, dAlpha, dLowBeta, dHighBeta, dGamma);

                            // Sets the value into the shared array
                            SetDownLowBeta(dLowBeta);

                            // Sets the value into the shared array
                            SetDownHighBeta(dHighBeta);
                        }
                        // Writes to log files
                        GameManager.rawBandPowersLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": LowBeta- Channel: " + channelListAlphaDowntrain[i] + ", value: " + dLowBeta[0]);
                        GameManager.rawBandPowersLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": HighBeta- Channel: " + channelListAlphaDowntrain[i] + ", value: " + dHighBeta[0]);

                        GameManager.averageBandPowersLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": LowBetaDown value: " + dLowBeta[0] + " | Current average: " + GetDownLowBetaAverage());
                        GameManager.averageBandPowersLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": HighBetaDown value: " + dHighBeta[0] + " | Current average: " + GetDownHighBetaAverage());
                    }

                    GameManager.averageBandPowersLog.WriteLine("----------------------Finished cycle----------------------");
                    GameManager.rawBandPowersLog.WriteLine("----------------------Finished cycle----------------------");
                }
                catch (Exception e)
                {
                    GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrEngineCalculations() - Error getting bandpowers: " + e.ToString());
                    Debug.LogError(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::ThrEngineCalculations() - Error getting bandpowers: " + e.ToString());
                    MenuUI.AddDialogToList(33);
                }

                // Restarts the stopwatch to control the next reading
                cycleControl.Stop();
                cycleControl = System.Diagnostics.Stopwatch.StartNew();
            }
        }
    }

    /// <summary>
    /// Stops the thread execution and closes connections.
    /// </summary>
    public static void RequestThreadStop()
    {
        bThrShouldStop = true;
        fullTimer.Stop();
        GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::RequestThreadStop() - Requested thread stop: " + fullTimer.ElapsedMilliseconds);
        try
        {
            engine.Disconnect();
        }
        catch (Exception e)
        {
            GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::RequestThreadStop() - Unable to disconnect headset engine. " + e.ToString());
            Debug.LogError(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::RequestThreadStop() - Unable to disconnect headset engine." + e.ToString());
        }

        if (bIsDebugEngine)
        {
            GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::RequestThreadStop() - Stopping Thread.");
            Debug.Log(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::RequestThreadStop() -  Stopping Thread.");
        }
    }

    #region Engine Event Listeners
    /// <summary>
    /// Inherited from EmoEngine. Event for headset connected.
    /// </summary>
    /// <param name="sender">Object sender.</param>
    /// <param name="e">EmoEngine Event.</param>
    private static void Engine_EmoEngineConnected(object sender, EmoEngineEventArgs e)
    {
        GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::Engine_EmoEngineConnected() - Emoengine connected.");
        MenuUI.AddDialogToList(30);
        if (bIsDebugEngine)
        {
            Debug.Log(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": Emoengine connected.");
        }
    }

    /// <summary>
    /// Inherited from EmoEngine. Event for headset disconnected.
    /// </summary>
    /// <param name="sender">Object sender.</param>
    /// <param name="e">EmoEngine Event.</param>
    private static void Engine_EmoEngineDisconnected(object sender, EmoEngineEventArgs e)
    {
        GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::Engine_EmoEngineDisconnected() - Emoengine disconnected.");
        MenuUI.AddDialogToList(31);
        if (bIsDebugEngine)
        {
            Debug.Log(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": Headset disconnected!");
        }
    }

    /// <summary>
    /// Inherited from EmoEngine. Event for engine user connected.
    /// </summary>
    /// <param name="sender">Object sender.</param>
    /// <param name="e">EmoEngine Event.</param>
    private static void Engine_UserAdded(object sender, EmoEngineEventArgs e)
    {
        GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::Engine_UserAdded() - User Added Event has occured");
        // MenuUI.AddDialogToList(34);
        userID = (int)e.userId;
        if (bIsDebugEngine)
        {
            Debug.Log(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": User added.");
        }
    }

    /// <summary>
    /// Inherited from EmoEngine. Event for engine user disconnected.
    /// </summary>
    /// <param name="sender">Object sender.</param>
    /// <param name="e">EmoEngine Event.</param>
    private static void Engine_UserRemoved(object sender, EmoEngineEventArgs e)
    {
        GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EngineManager::Engine_UserRemoved() - User removed event");
        // MenuUI.AddDialogToList(35);
        if (bIsDebugEngine)
        {
            Debug.Log(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": User removed.");
        }
    }
    #endregion
}