﻿//-----------------------------------------------------------------------
// <copyright file="DynamicDifficulty.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-9-9, 08:44</last_modified>
// <summary>Game Difficulty Management</summary>
//-----------------------------------------------------------------------
using System;
using UnityEngine;

/// <summary>
/// Game difficulty management.
/// </summary>
public class DynamicDifficulty : MonoBehaviour
{

    /// <summary>
    /// Internal variable to control debug log.
    /// </summary>
    private bool bIsDebugDifficulty = false;

    #region Numbers reached
    /// <summary>
    /// Max level reached while playing.
    /// </summary>
    private int iMaxDifficultyLevelReached;

    /// <summary>
    /// Max average reached while playing.
    /// </summary>
    private double dMaxUpAlphaAverageReached;

    /// <summary>
    /// Min average reached while playing.
    /// </summary>
    private double dMinDownAlphaAverageReached;

    /// <summary>
    /// Min average reached while playing.
    /// </summary>
    private double dMinDownLowBetaAverageReached;

    /// <summary>
    /// Min average reached while playing.
    /// </summary>
    private double dMinDownHighBetaAverageReached;
    #endregion

    #region Bandwidth Amplitude Limits
    /// <summary>
    /// Alpha uptrain min amplitude (uV).
    /// </summary>
    private static double dUpAlphaMinAmpl;

    /// <summary>
    /// Alpha uptrain max amplitude (uV).
    /// </summary>
    //private static double dUpAlphaMaxAmpl;

    /// <summary>
    /// Alpha downtrain min amplitude (uV).
    /// </summary>
    //private static double dDownAlphaMinAmpl;

    /// <summary>
    /// Alpha downtrain max amplitude (uV).
    /// </summary>
    private static double dDownAlphaMaxAmpl;

    /// <summary>
    /// Low beta downtrain min amplitude (uV).
    /// </summary>
    //private static double dDownLowBetaMinAmpl;

    /// <summary>
    /// Low beta downtrain max amplitude (uV).
    /// </summary>
    private static double dDownLowBetaMaxAmpl;

    /// <summary>
    /// High beta downtrain min amplitude (uV).
    /// </summary>
    //private static double dDownHighBetaMinAmpl;

    /// <summary>
    /// High beta downtrain max amplitude (uV).
    /// </summary>
    private static double dDownHighBetaMaxAmpl;
    #endregion

    #region Difficulty
    /// <summary>
    /// Current game difficulty.
    /// </summary>
    private static int iCurrentDifficultyLevel = 1;

    /// <summary>
    /// Game Power value for enemyhealth and character power calculations
    /// </summary>
    public static double dPowerValueForCalculations = 1.25;

    /// <summary>
    /// Enemyhealth calculation divisor
    /// </summary>
    public static double dEnemyHealthDivisor = 2;

    /// <summary>
    /// Difficulty up alpha window adjustment value.
    /// </summary>
    private double dUpAlphaDifficultyChangeValue;

    /// <summary>
    /// Difficulty down alpha window adjustment value.
    /// </summary>
    private double dDownAlphaDifficultyChangeValue;

    /// <summary>
    /// Difficulty low beta window adjustment value.
    /// </summary>
    private double dDownLowBetaDifficultyChangeValue;

    /// <summary>
    /// Difficulty high beta window adjustment value.
    /// </summary>
    private double dDownHighBetaDifficultyChangeValue;
    #endregion

    #region Execution control timer variables
    /// <summary>
    /// Stopwatch for correct behaviour registration.
    /// </summary>
    private System.Diagnostics.Stopwatch timeWinning;

    /// <summary>
    /// Stopwatch for incorrect behaviour registration.
    /// </summary>
    private System.Diagnostics.Stopwatch timeLosing;

    /// <summary>
    /// Necessary time to increase the difficulty level (ms).
    /// </summary>
    private int iTimeToIncreaseDifficulty = 10000;

    /// <summary>
    /// Necessary time to decrease the difficulty level (ms).
    /// </summary>
    private int iTimeToDecreaseDifficulty = 20000;

    /// <summary>
    /// Executes start of the difficulty managment only once.
    /// </summary>
    private bool bDynamicDifficultyStartedOnce;

    /// <summary>
    /// Controls start of the headset calculations thread only once.
    /// </summary>
    private bool bEngineManagerThreadStartedOnce;
    #endregion

    #region Bandwidth condition state
    /// <summary>
    /// Alpha uptrain values are met.
    /// </summary>
    private bool bIsAlphaUpCorrect = false;

    /// <summary>
    /// Alpha downtrain values are met.
    /// </summary>
    private bool bIsAlphaDownCorrect = false;

    /// <summary>
    /// LowBeta downtrain values are met.
    /// </summary>
    private bool bIsLowBetaDowCorrect = false;

    /// <summary>
    /// HighBeta downtrain values are met.
    /// </summary>
    private bool bIsHighBetaDowCorrect = false;
    #endregion

    #region Average readings
    /// <summary>
    /// Uptrain Alpha current average, The value is from calculating the average from the previous 2 seconds.
    /// </summary>
    private double dUpAlphaCurrentAverage;

    /// <summary>
    /// Downtrain Alpha current average, The value is from calculating the average from the previous 2 seconds.
    /// </summary>
    private double dDownAlphaCurrentAverage;

    /// <summary>
    /// Downtrain LowBeta current average, The value is from calculating the average from the previous 2 seconds.
    /// </summary>
    private double dDownLowBetaCurrentAverage;

    /// <summary>
    /// Downtrain HighBeta current average, The value is from calculating the average from the previous 2 seconds.
    /// </summary>
    private double dDownHighBetaCurrentAverage;
    #endregion

    #region Audio variables
    /// <summary>
    /// Audio source to play the rewardSound.
    /// </summary>
    private AudioSource asSoundSource;

    /// <summary>
    /// Audio reward sound.
    /// </summary>
    private AudioClip acRewardSound;

    /// <summary>
    /// Audio reward sound.
    /// </summary>
    private AudioClip acPenaltySound;
    #endregion

    public static int GetCurrentDifficultyLevel()
    {
        return iCurrentDifficultyLevel;
    }


    /// <summary>
    /// Inherited from MonoBehaviour. Use this for initialization.
    /// </summary>
    void Start()
    {
        timeWinning = new System.Diagnostics.Stopwatch();
        timeLosing = new System.Diagnostics.Stopwatch();

        bDynamicDifficultyStartedOnce = false;
        bEngineManagerThreadStartedOnce = false;

        // Initializes limits with 0
        dUpAlphaMinAmpl = 0;
        dDownAlphaMaxAmpl = 0;
        dDownLowBetaMaxAmpl = 0;
        dDownHighBetaMaxAmpl = 0;

        // Initializes the rewarding sound
        asSoundSource = gameObject.AddComponent<AudioSource>();
        try
        {
            acRewardSound = Resources.Load<AudioClip>("Audio/Sonic_Ring_Sound");
            acPenaltySound = Resources.Load<AudioClip>("Audio/Sonic_Penalty_Sound");
        }
        catch (Exception e)
        {
            GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": DynamicDifficulty::Start() - Unable to load AudioClip." + e.ToString());
            Debug.LogError(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": DynamicDifficulty::Start() - Unable to load AudioClip." + e.ToString());
        }

        // Sets starting difficulty in UI element
        MenuUI.tDifficultyText.text = iCurrentDifficultyLevel.ToString();
    }

    void Update()
    {
        // If countdown started and thread has not started yet
        if (MenuUI.bIsCountdownDialogActive && !bEngineManagerThreadStartedOnce)
        {
            bEngineManagerThreadStartedOnce = true;

            // Starts headset engine.
            EngineManager.StartReadingsProcedure();
        }

        // Checks if adjustment is finished to start dynamic difficulty
        if (!bDynamicDifficultyStartedOnce && EngineManager.GetIsAdjustmentFinished())
        {
            StartsDynamicDifficulty();

            // Sets flag to true to execute only once
            bDynamicDifficultyStartedOnce = true;
        }
    }

    private void StartsDynamicDifficulty()
    {
        GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": DynamicDifficulty::StartsDynamicDifficulty() - Starting dynamic difficulty adjustment.");

        if (bIsDebugDifficulty)
        {
            Debug.Log("Starting dynamic difficulty");
        }


        // Sets bandwidth amplitude limits
        // If UpAlpha min is below 0, sets to 0
        if (EngineManager.GetUpAlphaAdjustmentAverage() - EngineManager.GetUpAlphaStdDeviation() < 0)
        {
            dUpAlphaMinAmpl = 0;
        }
        else
        {
            dUpAlphaMinAmpl = EngineManager.GetUpAlphaAdjustmentAverage() - EngineManager.GetUpAlphaStdDeviation();
        }

        dDownAlphaMaxAmpl = EngineManager.GetDownAlphaAdjustmentAverage() + EngineManager.GetDownAlphaStdDeviation();
        dDownLowBetaMaxAmpl = EngineManager.GetDownLowBetaAdjustmentAverage() + EngineManager.GetDownLowBetaStdDeviation();
        dDownHighBetaMaxAmpl = EngineManager.GetDownHighBetaAdjustmentAverage() + EngineManager.GetDownHighBetaStdDeviation();

        // Sets bandwidth increment/decrement values
        dUpAlphaDifficultyChangeValue = EngineManager.GetUpAlphaStdDeviation() / 4;
        dDownAlphaDifficultyChangeValue = EngineManager.GetDownAlphaStdDeviation() / 4;
        dDownLowBetaDifficultyChangeValue = EngineManager.GetDownLowBetaStdDeviation() / 4;
        dDownHighBetaDifficultyChangeValue = EngineManager.GetDownHighBetaStdDeviation() / 4;


        // Log values into logfile
        GameManager.adjustmentPeriodLog.WriteLine("------------------------------------------------------------------------------");
        GameManager.adjustmentPeriodLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": DynamicDifficulty::StartsDynamicDifficulty() - Limits set");
        GameManager.adjustmentPeriodLog.WriteLine("dUpAlphaMinAmpl set: " + dUpAlphaMinAmpl);
        GameManager.adjustmentPeriodLog.WriteLine("dDownAlphaMaxAmpl set: " + dDownAlphaMaxAmpl);
        GameManager.adjustmentPeriodLog.WriteLine("dDownLowBetaMaxAmpl set: " + dDownLowBetaMaxAmpl);
        GameManager.adjustmentPeriodLog.WriteLine("dDownHighBetaMaxAmpl set: " + dDownHighBetaMaxAmpl);
        GameManager.adjustmentPeriodLog.WriteLine("dUpAlphaDifficultyChangeValue set: " + dUpAlphaDifficultyChangeValue);
        GameManager.adjustmentPeriodLog.WriteLine("dDownAlphaDifficultyChangeValue set: " + dDownAlphaDifficultyChangeValue);
        GameManager.adjustmentPeriodLog.WriteLine("dDownLowBetaDifficultyChangeValue set: " + dDownLowBetaDifficultyChangeValue);
        GameManager.adjustmentPeriodLog.WriteLine("dDownHighBetaDifficultyChangeValue set: " + dDownHighBetaDifficultyChangeValue);

        // If debug is active logs to console
        if (bIsDebugDifficulty)
        {
            Debug.Log("dUpAlphaMinAmpl set: " + dUpAlphaMinAmpl);
            Debug.Log("dDownAlphaMaxAmpl set: " + dDownAlphaMaxAmpl);
            Debug.Log("dDownLowBetaMaxAmpl set: " + dDownLowBetaMaxAmpl);
            Debug.Log("dDownHighBetaMaxAmpl set: " + dDownHighBetaMaxAmpl);
            Debug.Log("dUpAlphaDifficultyChangeValue set: " + dUpAlphaDifficultyChangeValue);
            Debug.Log("dDownAlphaDifficultyChangeValue set: " + dDownAlphaDifficultyChangeValue);
            Debug.Log("dDownLowBetaDifficultyChangeValue set: " + dDownLowBetaDifficultyChangeValue);
            Debug.Log("dDownHighBetaDifficultyChangeValue set: " + dDownHighBetaDifficultyChangeValue);
        }

        // Starts process of adjusting difficulty from 0.5 the cycle
        InvokeRepeating("CalculateDynamicDifficulty", 0, 0.5f);
    }



    /// <summary>
    /// Gets the bandwidth averages and make correspondent difficulty adjustment. Runs 2 times per second.
    /// </summary>
    void CalculateDynamicDifficulty()
    {
        
        GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": Started dynamic difficulty calculation.");

        // Gets current averages
        dUpAlphaCurrentAverage = EngineManager.GetUpAlphaAverage();
        dDownAlphaCurrentAverage = EngineManager.GetDownAlphaAverage();
        dDownLowBetaCurrentAverage = EngineManager.GetDownLowBetaAverage();
        dDownHighBetaCurrentAverage = EngineManager.GetDownHighBetaAverage();

        // Verifies if it's max/min average got
        if(dUpAlphaCurrentAverage > dMaxUpAlphaAverageReached)
        {
            dMaxUpAlphaAverageReached = dUpAlphaCurrentAverage;
        }

        if(dDownAlphaCurrentAverage < dMinDownAlphaAverageReached)
        {
            dMinDownAlphaAverageReached = dDownAlphaCurrentAverage;
        }

        if(dDownLowBetaCurrentAverage < dMinDownLowBetaAverageReached)
        {
            dMinDownLowBetaAverageReached = dDownLowBetaCurrentAverage;
        }

        if(dDownHighBetaCurrentAverage < dMinDownHighBetaAverageReached)
        {
            dMinDownHighBetaAverageReached = dDownHighBetaCurrentAverage;
        }


        // Checks if thread is still alive
        if (EngineManager.tReadingsThread != null && EngineManager.tReadingsThread.IsAlive)
        {
            if (bIsDebugDifficulty)
            {
                Debug.Log("Current up/down alpha/beta average got: " + dUpAlphaCurrentAverage + " / " + dDownAlphaCurrentAverage + " / " + dDownLowBetaCurrentAverage + " / " + dDownHighBetaCurrentAverage);
                Debug.Log("Most recent readings: " + EngineManager.GetLastUpAlpha() + " - " + EngineManager.GetLastDownAlpha() + " - " + EngineManager.GetLastDownLowBeta() + " - " + EngineManager.GetLastDownHighBeta());

                // Sets current limits
                // Debug.Log("Current difficulty: " + iCurrentDifficultyLevel + "Current limits: AUm:" + dUpAlphaMinAmpl + " AUM:" + dUpAlphaMaxAmpl + " ADm:" + dDownAlphaMinAmpl + " ADM:" + dDownAlphaMaxAmpl + " LBDm:" + dDownLowBetaMinAmpl + " LBDM:" + dDownLowBetaMaxAmpl + " HBDm:" + dDownHighBetaMinAmpl + " HBDM:" + dDownHighBetaMaxAmpl);
                Debug.Log("Current difficulty: " + iCurrentDifficultyLevel + "Current limits: AUm:" + dUpAlphaMinAmpl + " ADM:" + dDownAlphaMaxAmpl + " LBDM:" + dDownLowBetaMaxAmpl + " HBDM:" + dDownHighBetaMaxAmpl);
            }

            // Dynamic adjustment
            // In the case the player is in the correct behaviour
            if (CheckAllConditionsAreMet())
            {
                // Checks if losing stopwatch is running and stops if true
                if (timeLosing.IsRunning)
                {
                    timeLosing.Stop();
                }

                // Checks if winning stopwatch is already running and starts if not.
                if (!timeWinning.IsRunning)
                {
                    timeWinning = System.Diagnostics.Stopwatch.StartNew();
                }

                // Tries to hit enemy procedure.
                HitEnemyIfExists();

                // If player has correct behaviour for the set time, increases difficulty and resets stopwatch.
                if (timeWinning.ElapsedMilliseconds > iTimeToIncreaseDifficulty)
                {
                    // Plays reward sound.
                    asSoundSource.PlayOneShot(acRewardSound);

                    IncreaseDifficulty();
                    timeWinning = System.Diagnostics.Stopwatch.StartNew();
                }
            }
            else
            {   // Player is not having the expected behaviour and if takes too long the difficulty is decreased
                // Checks if winning stopwatch is running and stops if true.
                if (timeWinning.IsRunning)
                {
                    timeWinning.Stop();
                }

                // Checks if losing stopwatch is already running and starts if not.
                if (!timeLosing.IsRunning)
                {
                    timeLosing = System.Diagnostics.Stopwatch.StartNew();
                }

                // If player has incorrect behaviour for the set time, decreases difficulty and resets stopwatch
                if (timeLosing.ElapsedMilliseconds > iTimeToDecreaseDifficulty && iCurrentDifficultyLevel > 1)
                {
                    // Plays penalty sound.
                    asSoundSource.PlayOneShot(acPenaltySound);

                    DecreaseDifficulty();
                    timeLosing = System.Diagnostics.Stopwatch.StartNew();
                }
            }
        }

        // Sets difficulty in UI element
        MenuUI.tDifficultyText.text = iCurrentDifficultyLevel.ToString();
    }

    /// <summary>
    /// If there is an anemy in the scene, reduces it's health.
    /// </summary>
    private void HitEnemyIfExists()
    {
        GameObject goEnemy = null;

        // Tries to get current enemy gameobject
        try
        {
            goEnemy = GameObject.FindGameObjectWithTag("Enemy");
        }
        catch (Exception e)
        {
            GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": DynamicDifficulty::HitEnemyIfExists() - Unable to find enemy game object." + e.ToString());
            Debug.LogError(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": DynamicDifficulty::HitEnemyIfExists() - Unable to find enemy game object." + e.ToString());
        }

        // Does enemy actual damage
        if (goEnemy != null && goEnemy.GetComponent<EnemyStatsnSettings>() != null)
        {
            if (goEnemy.GetComponent<EnemyStatsnSettings>().bCanBeHit)
            {
                goEnemy.GetComponent<EnemyStatsnSettings>().SetdCurrentHealth(goEnemy.GetComponent<EnemyStatsnSettings>().GetdCurrentHealth() - CharStatsnSettings.dCurrentAtkPower);
            }
        }
    }

    /// <summary>
    /// Verifies if current values are within bandwidht thresholds.
    /// </summary>
    /// <returns>True if all bandwidth readings are correct.</returns>
    private bool CheckAllConditionsAreMet()
    {
        if (dUpAlphaCurrentAverage > dUpAlphaMinAmpl /*&& dUpAlphaCurrentAverage < dUpAlphaMaxAmpl*/)
        {
            bIsAlphaUpCorrect = true;
            //if (bIsDebugDifficulty)
            //{
            //    Debug.Log("Up Alpha is correct.");
            //}
        }
        else
        {
            bIsAlphaUpCorrect = false;
        }

        if (/*dDownAlphaCurrentAverage > dDownAlphaMinAmpl &&*/ dDownAlphaCurrentAverage < dDownAlphaMaxAmpl)
        {
            bIsAlphaDownCorrect = true;
            //if (bIsDebugDifficulty)
            //{
            //    Debug.Log("Down Alpha is correct.");
            //}
        }
        else
        {
            bIsAlphaDownCorrect = false;
        }

        if (/*dDownLowBetaCurrentAverage > dDownLowBetaMinAmpl &&*/ dDownLowBetaCurrentAverage < dDownLowBetaMaxAmpl)
        {
            bIsLowBetaDowCorrect = true;
            //if (bIsDebugDifficulty)
            //{
            //    Debug.Log("LowBeta is correct.");
            //}
        }
        else
        {
            bIsLowBetaDowCorrect = false;
        }

        if (/*dDownHighBetaCurrentAverage > dDownHighBetaMinAmpl &&*/ dDownHighBetaCurrentAverage < dDownLowBetaMaxAmpl)
        {
            bIsHighBetaDowCorrect = true;
            //if (bIsDebugDifficulty)
            //{
            //    Debug.Log("HighBeta is correct.");
            //}
        }
        else
        {
            bIsHighBetaDowCorrect = false;
        }

        if (bIsAlphaUpCorrect && bIsAlphaDownCorrect && bIsLowBetaDowCorrect && bIsHighBetaDowCorrect)
        {
            //if (bIsDebugDifficulty)
            //{
            //    Debug.Log("All waves are correct.");
            //}
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Increases current difficulty level by adjust the minimum or maximum amplitude expected.
    /// </summary>
    private void IncreaseDifficulty()
    {
        if(bIsDebugDifficulty)
        {
            Debug.Log("Increasing difficulty.");
        }

        iCurrentDifficultyLevel++;
        dUpAlphaMinAmpl += dUpAlphaDifficultyChangeValue;
        if (dDownAlphaMaxAmpl - dDownAlphaDifficultyChangeValue < 0)
        {
            dDownAlphaMaxAmpl = 0.1f;
        }
        else
        {
            dDownAlphaMaxAmpl -= dDownAlphaDifficultyChangeValue;
        }

        if (dDownLowBetaMaxAmpl - dDownLowBetaDifficultyChangeValue < 0.1f)
        {
            dDownLowBetaMaxAmpl = 0.1f;
        }
        else
        {
            dDownLowBetaMaxAmpl -= dDownLowBetaDifficultyChangeValue;
        }

        if (dDownHighBetaMaxAmpl - dDownHighBetaDifficultyChangeValue < 0.1f)
        {
            dDownHighBetaMaxAmpl = 0.1f;
        }
        else
        {
            dDownHighBetaMaxAmpl -= dDownHighBetaDifficultyChangeValue;
        }
        if(iMaxDifficultyLevelReached < iCurrentDifficultyLevel)
        {
            iMaxDifficultyLevelReached = iCurrentDifficultyLevel;
        }

        GameManager.averageBandPowersLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": DynamicDifficulty::IncreaseDifficulty() - Increasing difficulty to level: " + iCurrentDifficultyLevel);
        GameManager.averageBandPowersLog.WriteLine("              dUpAlphaMinAmpl  dDownAlphaMaxAmpl dDownLowBetaMaxAmpl dDownHighBetaMaxAmpl");
        GameManager.averageBandPowersLog.WriteLine("New limits: " + dUpAlphaMinAmpl + " " + dDownAlphaMaxAmpl + " " + dDownLowBetaMaxAmpl + " " + dDownHighBetaMaxAmpl + "\n");
    }

    /// <summary>
    /// Decreases current difficulty level by adjust the minimum or maximum amplitude expected.
    /// </summary>
    private void DecreaseDifficulty()
    {
        if (bIsDebugDifficulty)
        {
            Debug.Log("Decreasing difficulty.");
        }

        if (iCurrentDifficultyLevel > 0)
        {
            iCurrentDifficultyLevel--;
            if (dUpAlphaMinAmpl - dUpAlphaDifficultyChangeValue < 0)
            {
                dUpAlphaMinAmpl = 0; ;
            }
            else
            {
                dUpAlphaMinAmpl -= dUpAlphaDifficultyChangeValue;
            }
            dDownAlphaMaxAmpl += dDownAlphaDifficultyChangeValue;
            dDownLowBetaMaxAmpl += dDownLowBetaDifficultyChangeValue;
            dDownHighBetaMaxAmpl += dDownHighBetaDifficultyChangeValue;

            GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": DynamicDifficulty::DecreaseDifficulty() - Decreasing difficulty to level: " + iCurrentDifficultyLevel);
            GameManager.gameEngineLog.WriteLine("Values columns: dUpAlphaMinAmpl  dDownAlphaMaxAmpl dDownLowBetaMaxAmpl dDownHighBetaMaxAmpl");
            GameManager.gameEngineLog.WriteLine("New values: " + dUpAlphaMinAmpl + " " + dDownAlphaMaxAmpl + " " + dDownLowBetaMaxAmpl + " " + dDownHighBetaMaxAmpl);
        }
    }

    /// <summary>
    /// Logs final execution stats.
    /// </summary>
    public void SetFinalInfoForLog()
    {
        // When object is destroyed logs values gotten
        GameManager.averageBandPowersLog.WriteLine("--------------------------------------------------------");
        GameManager.averageBandPowersLog.WriteLine("DynamicDifficulty::SetFinalInfoForLog() - Max/Min values reached:");
        GameManager.averageBandPowersLog.WriteLine("Max level reached: " + iMaxDifficultyLevelReached);
        GameManager.averageBandPowersLog.WriteLine("Max average UpAlpha: " + dMaxUpAlphaAverageReached);
        GameManager.averageBandPowersLog.WriteLine("Min average DownAlpha: " + dMinDownAlphaAverageReached);
        GameManager.averageBandPowersLog.WriteLine("Min average DownLowBeta: " + dMinDownLowBetaAverageReached);
        GameManager.averageBandPowersLog.WriteLine("Min average DownHighBeta: " + dMinDownHighBetaAverageReached);
    }

}
