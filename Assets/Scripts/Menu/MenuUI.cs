﻿//-----------------------------------------------------------------------
// <copyright file="MenuUI.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-21, 16:02</last_modified>
// <summary>Menus and dialog management.</summary>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Menu options management.
/// </summary>
public class MenuUI : MonoBehaviour
{
    private bool bIsInDebugMode = false;

    /// <summary>
    /// Dialog window GameObject.
    /// </summary>
    public static GameObject goDialogPanel;

    /// <summary>
    /// Countdown dialog window GameObject.
    /// </summary>
    public static GameObject goDialogCountdownPanel;

    /// <summary>
    /// Dialog text component.
    /// </summary>
    public static Text tDialogText;

    /// <summary>
    /// Countdown dialog text component.
    /// </summary>
    public static Text tCountdownDialogText;

    /// <summary>
    /// Dialog window GameObject.
    /// </summary>
    public static GameObject goDifficultyPanel;

    /// <summary>
    /// Dialog text component.
    /// </summary>
    public static Text tDifficultyText;

    /// <summary>
    /// Dialog window GameObject.
    /// </summary>
    public static GameObject goEnemiesRemainingPanel;

    /// <summary>
    /// Dialog text component.
    /// </summary>
    public static Text tEnemiesRemainingText;

    /// <summary>
    /// Pause menu GameObject.
    /// </summary>
    public GameObject goPausePanel;

    #region DialogImporter Variables
    /// <summary>
    /// Resources textfile with dialogs.
    /// </summary>
    public TextAsset textFile;

    /// <summary>
    /// Raw text file lines.
    /// </summary>
    private string[] sRawTextLines;

    /// <summary>
    /// Available dialogs list from textfile.
    /// </summary>
    private static List<string> sDialogs;

    /// <summary>
    /// Start of the dialog position.
    /// </summary>
    private int iLineStartPos = 0;

    /// <summary>
    /// End of the dialog position.
    /// </summary>
    private int iLineEndPos = 0;
    #endregion

    /// <summary>
    /// To activate countdown as soon as the dialog disappears.
    /// </summary>
    public static bool bGoingToActivateCountdown = false;

    /// <summary>
    /// To deactivate countdown.
    /// </summary>
    public static bool bCountdownFinished = false;

    /// <summary>
    /// If dialog is active.
    /// </summary>
    private static bool bIsDialogActive;

    /// <summary>
    /// If Countdown is running.
    /// </summary>
    public static bool bIsCountdownDialogActive;

    /// <summary>
    /// Current countdown position.
    /// </summary>
    private int iCurrentCountdownIndex = 18;

    /// <summary>
    /// Timer in miliseconds, to accelerate the debug.
    /// </summary>
    private int iCountdownWaitTime;

    /// <summary>
    /// Stopwatch for countdown.
    /// </summary>
    private System.Diagnostics.Stopwatch countdownTimer = new System.Diagnostics.Stopwatch();

    /// <summary>
    /// List of dialogs that will be shown.
    /// </summary>
    private static List<int> lDialogIndexList = new List<int>();

    /// <summary>
    /// If game is paused.
    /// </summary>
    private bool bIsPaused;

    /// <summary>
    /// Variable to store the current active scene number.
    /// </summary>
    private int iActiveSceneNum = 0;

    /// <summary>
    /// Loads selected scene in the editor button.
    /// </summary>
    /// <param name="iNewScene">New scene index to load.</param>
    public void LoadScene(int iNewScene)
    {
        SceneManager.LoadScene(iNewScene);
    }

    #region Dialog Management
    /// <summary>
    /// Public method to add dialog to list.
    /// </summary>
    /// <param name="newDialogIndex">One or more dialog index to add.</param>
    public static void AddDialogToList(params int[] newDialogIndex)
    {
        // Adds dialog index to list.
        for (int i = 0; i < newDialogIndex.Length; i++)
        {
            // If index passed exists inside the file adds to list.
            if (sDialogs.Count >= newDialogIndex[i])
            {
                lDialogIndexList.Add(newDialogIndex[i]);
            }
        }
    }

    /// <summary>
    /// Shows a dialog with a set text from the index of the dialogs textfile. Can be called from any class.
    /// </summary>
    /// <param name = "iDialogIndex">index of the dialog in the dialog textfile.</param>
    public static void ShowSingleDialogByIndex(int iDialogIndex)
    {
        // If dialog is not active and index passed is within range
        if (!bIsDialogActive && sDialogs.Count >= iDialogIndex)
        {
            goDialogPanel.SetActive(true);
            Time.timeScale = 0.0f;      // Pause game while message is shown
            tDialogText.text = sDialogs[iDialogIndex];
            bIsDialogActive = true;
        }
    }

    /// <summary>
    /// Shows a dialog with a set text. Can be called from any class.
    /// </summary>
    /// <param name = "sMessage">String dialog to show.</param>
    public static void ShowSingleDialogByString(string sMessage)
    {
        if (!bIsDialogActive)
        {
            goDialogPanel.SetActive(true);
            Time.timeScale = 0.0f;      // Pause game while message is shown
            tDialogText.text = sMessage;
            bIsDialogActive = true;
        }
    }

    /// <summary>
    /// Shows a list of dialogues from the index of the dialogs textfile. Can be called from any class.
    /// </summary>
    public static void ShowDialog()
    {
        // If dialog is not active and index passed is within range
        if (!bIsDialogActive)
        {
            goDialogPanel.SetActive(true);
            Time.timeScale = 0.0f;      // Pause game while message is shown

            // Prints first message in the array
            tDialogText.text = sDialogs[lDialogIndexList[0]];
            bIsDialogActive = true;

            // Removes current shown dialog from list
            lDialogIndexList.RemoveAt(0);
        }
    }

    /// <summary>
    /// Disables the dialog and resets the variables.
    /// </summary>
    public void DisableDialog()
    {
        goDialogPanel.SetActive(false);
        tDialogText.text = string.Empty;
        Time.timeScale = 1.0f;      // Returns game to normal speed
        bIsDialogActive = false;
    }

    /// <summary>
    /// Start countdown for adjustment readings.
    /// </summary>
    private void BeginCountdown()
    {
        if (!bIsCountdownDialogActive)
        {
            goDialogCountdownPanel.SetActive(true);
            Time.timeScale = 0.0f;
            countdownTimer.Start();
            bIsCountdownDialogActive = true;
        }
    }

    /// <summary>
    /// Disables the countdown dialog and resets the variables.
    /// </summary>
    public void DisableCountdown()
    {
        goDialogCountdownPanel.SetActive(false);
        tCountdownDialogText.text = string.Empty;
        Time.timeScale = 1.0f;      // Returns game to normal speed
        countdownTimer.Stop();
        AddDialogToList(49);
        bIsCountdownDialogActive = false;
    }
    #endregion

    /// <summary>
    /// Toogle pause the game.
    /// </summary>
    public void TogglePauseGame()
    {
        if (!bIsPaused)
        {
            Time.timeScale = 0.0f;      // Pause; stops time in game and set pause panel visible
            goPausePanel.SetActive(true);
            bIsPaused = true;
        }
        else
        {
            goPausePanel.SetActive(false);
            Time.timeScale = 1.0f;      // Unpause; returns game to normal speed and set pause panel invisible
            bIsPaused = false;
        }
    }

    ///// <summary>
    ///// Exits game application.
    ///// </summary>
    //public static void QuitGame()
    //{
    //    GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + "MenuUI::QuitGame() - Exiting game.");
    //    Debug.Log("MenuUI::QuitGame() - Exiting game.");
    //    if (iActiveSceneNum == 1 && EngineManager.tReadingsThread != null && EngineManager.tReadingsThread.IsAlive)
    //    {
    //        EngineManager.RequestThreadStop();
    //    }
    //    Gamemanager.CloseLogs();
    //    Application.Quit();
    //}


    /// <summary>
    /// Exits game application.
    /// </summary>
    public void QuitGame()
    {
        GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + "MenuUI::QuitGame() - Exiting game.");
        Debug.Log("MenuUI::QuitGame() - Exiting game.");
        if (iActiveSceneNum == 1 && EngineManager.tReadingsThread != null && EngineManager.tReadingsThread.IsAlive)
        {
            EngineManager.RequestThreadStop();
            GameManager.CloseLogFiles();
        }

        Application.Quit();
    }

    ///// <summary>
    ///// QuitButton OnClick wrapper. Necessary for function to be available for quitButton.
    ///// </summary>
    //public void ButtonQuitGame()
    //{
    //    QuitGame();
    //}

    /// <summary>
    /// Inherited from MonoBehaviour. Use this for initialization.
    /// </summary>
    void Awake()
    {
        // Gets current active scene for element control.
        iActiveSceneNum = SceneManager.GetActiveScene().buildIndex;

        if (iActiveSceneNum == 1)
        {
            bIsPaused = false;
            bIsDialogActive = false;
            bIsCountdownDialogActive = false;

            // FIXME Temporary workaround to get the reference to the dialogPanel
            // Finds active gameObject
            goDialogPanel = GameObject.FindGameObjectWithTag("Dialog");
            goDifficultyPanel = GameObject.FindGameObjectWithTag("Difficulty");
            goDialogCountdownPanel = GameObject.FindGameObjectWithTag("Countdown");
            goEnemiesRemainingPanel = GameObject.FindGameObjectWithTag("EnemiesToEnd");

            // Gets the text of the dialog
            tDialogText = goDialogPanel.GetComponentInChildren<Text>();
            tDifficultyText = goDifficultyPanel.GetComponentInChildren<Text>();
            tCountdownDialogText = goDialogCountdownPanel.GetComponentInChildren<Text>();
            tEnemiesRemainingText = goEnemiesRemainingPanel.GetComponentInChildren<Text>();

            // Inactivates the object
            goDialogPanel.SetActive(false);
            goDialogCountdownPanel.SetActive(false);
        }

        // Initializes the dialog list.
        sDialogs = new List<string>();
        // Verifies if textfile exists .
        if (textFile != null)
        {
            // Gets raw lines in textfile.
            sRawTextLines = textFile.text.Split('\n');

            // For each line gets the message inside [].
            foreach (string line in sRawTextLines)
            {
                iLineStartPos = line.LastIndexOf("[") + 1;
                iLineEndPos = line.LastIndexOf("]");

                // Adds the message to the string list to be available.
                sDialogs.Add(line.Substring(iLineStartPos, iLineEndPos - iLineStartPos));
            }
        }

        countdownTimer = new System.Diagnostics.Stopwatch();
        if (bIsInDebugMode)
        {
            iCountdownWaitTime = 1;
        }
        else
        {
            iCountdownWaitTime = 1000;
        }
    }

    /// <summary>
    /// Inherited from MonoBehaviour. Update is called once per frame.
    /// </summary>
    void Update()
    {
        // Esc key press pauses game
        if (iActiveSceneNum == 1 && Input.GetButtonDown("Cancel"))
        {
            TogglePauseGame();
        }

        if (Input.GetKeyDown(KeyCode.Space) && bIsDialogActive)
        {
            DisableDialog();
            if (lDialogIndexList.Count > 0)
            {
                ShowDialog();
            }
        }

        // Verifies if there is any dialog present in the list to be printed
        if (lDialogIndexList.Count > 0)
        {
            ShowDialog();
        }

        // If dialog is not present and an indication to activate countdown was made.
        if(!bIsDialogActive && bGoingToActivateCountdown)
        {
            BeginCountdown();
            bGoingToActivateCountdown = false;
        }

        // Manages countdown number change
        if (bIsCountdownDialogActive && countdownTimer.ElapsedMilliseconds >= iCountdownWaitTime && iCurrentCountdownIndex >= 0)
        {
            tCountdownDialogText.text = sDialogs[iCurrentCountdownIndex];
            iCurrentCountdownIndex--;
            countdownTimer.Stop();
            countdownTimer = System.Diagnostics.Stopwatch.StartNew();
        }

        // If countdown is finished
        if(bIsCountdownDialogActive && countdownTimer.ElapsedMilliseconds >= iCountdownWaitTime && iCurrentCountdownIndex < 0)
        {
            DisableCountdown();
        }
    }
}
