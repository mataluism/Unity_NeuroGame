﻿//-----------------------------------------------------------------------
// <copyright file="MapGenerator.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-21, 10:57</last_modified>
// <summary>Random map generation operations.</summary>
//-----------------------------------------------------------------------
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Random map generation class.
/// </summary>
public class MapGenerator
{
    /// <summary>
    /// Total number of rooms in the level -1 (needs to be larger than 4!).
    /// </summary>
    public int iNumRooms = 9;

    /// <summary>
    /// Created rooms list.
    /// </summary>
    private List<Room> lRooms = new List<Room>();

    /// <summary>
    /// Room positioning matrix. Used for calculations to avoid room overlap. 0=no map, 1=map.
    /// </summary>
    private int[,] arrayWorldMap;

    /// <summary>
    /// Current X of WorldMap matrix.
    /// </summary>
    private int iArrayWorldMapGenX;

    /// <summary>
    /// Current Y of WorldMap matrix.
    /// </summary>
    private int iArrayWorldMapGenY;

    #region Constructors/MapGenerator
    /// <summary>
    /// Initializes a new instance of the <see cref="MapGenerator"/> class. Used to randomly generate the map.
    /// </summary>
    public MapGenerator()
    {
        arrayWorldMap = new int[((iNumRooms * 2) + 2), ((iNumRooms * 2) + 2)];        // The maximum height and with of the full map is at most, 2 times the num of rooms

        // Fills empty(0) arrayWorldMap
        for (int i = 0; i < arrayWorldMap.GetLength(0); i++)
        {
            for (int j = 0; j < arrayWorldMap.GetLength(1); j++)
            {
                arrayWorldMap[i, j] = 0;
            }
        }

        // FirstRoom
        // Create first room which will be set in the center of World
        Room tempRoom = new Room();

        // Sets the arrayWorldMap corresponding middle position
        arrayWorldMap[iNumRooms, iNumRooms] = 1;
        iArrayWorldMapGenY = iNumRooms;
        iArrayWorldMapGenX = iNumRooms;

        // Gets exit door position randomly
        Vector2 v2tempDoorPos;
        GameManager.Direction exitDorPos = GetRandomExitDoorPosition(arrayWorldMap, iArrayWorldMapGenX, iArrayWorldMapGenY);
        Debug.LogWarning("Exit door: " + exitDorPos);

        switch (exitDorPos)
        {
            case GameManager.Direction.Right:    // Door right
                {
                    v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x + (tempRoom.GetRoomWidth() / 2);
                    v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y;
                    tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Right);
                }

                break;
            case GameManager.Direction.Up: // Door up
                {
                    v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x;
                    v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y + (tempRoom.GetRoomHeight() / 2);
                    tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Up);
                }

                break;
            case GameManager.Direction.Left: // Door left
                {
                    v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x - (tempRoom.GetRoomWidth() / 2);
                    v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y;
                    tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Left);
                }

                break;
            case GameManager.Direction.Down: // Door down
                {
                    v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x;
                    v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y - (tempRoom.GetRoomHeight() / 2);
                    tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Down);
                }

                break;
        }

        lRooms.Add(tempRoom);

        // for debug
        // PrintArrayWorldMap(arrayWorldMap);

        // Cycle to set all the rooms necessary
        for (int i = 0; i < iNumRooms; i++)
        {
            tempRoom = new Room();  // Creates a new room with default parameters

            // Gets previous room exit door to manage new room position
            GameManager.Direction previousRoomExitDoorDirection = lRooms[i].GetRoomExitDoorDirection();
            
            // New room is up
            if (previousRoomExitDoorDirection == GameManager.Direction.Up)
            {
                // Debug.Log("Room Up");

                // Gets previous room origin and height to calculate new room origin and door positions
                Vector2 previousRoomOrg = lRooms[i].GetRoomOriginPosition();    // Previous room origin
                float fPreviousRoomHeight = lRooms[i].GetRoomHeight();               // Previous room height
                float fCurrentRoomHeight = tempRoom.GetRoomHeight();     // Currently created room height

                // Gets the previous width so that the new room has no more than it; to avoid small corner overlaps
                tempRoom.SetRoomWidth(lRooms[i].GetRoomWidth());

                // Sets up new room origin
                // If current room is Up, new origin is equal to the last origin plus half the height of the previous and current room
                tempRoom.SetRoomOriginPosition(new Vector2(previousRoomOrg.x, previousRoomOrg.y + (fPreviousRoomHeight / 2.0f) + (fCurrentRoomHeight / 2.0f)));

                // Sets entry door position = exit door from previous room
                // Vector2 tempDoorPos;
                v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x;
                v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y - (tempRoom.GetRoomHeight() / 2);
                tempRoom.SetRoomEntryDoorPosition(v2tempDoorPos, GameManager.Direction.Down);

                iArrayWorldMapGenY -= 1; // If room is up sub one to array X pos and sets position occupied
                arrayWorldMap[iArrayWorldMapGenX, iArrayWorldMapGenY] = 1;

                // Gets exitdoor position randomly
                exitDorPos = GetRandomExitDoorPosition(arrayWorldMap, iArrayWorldMapGenX, iArrayWorldMapGenY);
                Debug.LogWarning("Exit door: " + exitDorPos);

                switch (exitDorPos)
                {
                    case GameManager.Direction.Right:    // Door right
                        {
                            v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x + (tempRoom.GetRoomWidth() / 2);
                            v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y;
                            tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Right);
                        }

                        break;
                    case GameManager.Direction.Up: // Door up
                        {
                            v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x;
                            v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y + (tempRoom.GetRoomHeight() / 2);
                            tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Up);
                        }

                        break;
                    case GameManager.Direction.Left: // Door left
                        {
                            v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x - (tempRoom.GetRoomWidth() / 2);
                            v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y;
                            tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Left);
                        }

                        break;
                }
            }

            // New room is down
            if (previousRoomExitDoorDirection == GameManager.Direction.Down)
            {
                // Debug.Log("Room Down");

                // Gets previous room origin and height to calculate new room origin and door positions
                Vector2 previousRoomOrg = lRooms[i].GetRoomOriginPosition();    // Previous room origin
                float fPreviousRoomHeight = lRooms[i].GetRoomHeight();               // Previous room height
                float fCurrentRoomHeight = tempRoom.GetRoomHeight();     // Currently created room height

                // Gets the previous width so that the new room has no more than it; to avoid small corner overlaps
                tempRoom.SetRoomWidth(lRooms[i].GetRoomWidth());

                // Sets up new room origin
                // If current room is Down, new origin is equal to the last origin minus half the height of the previous and current room
                tempRoom.SetRoomOriginPosition(new Vector2(previousRoomOrg.x, previousRoomOrg.y - (fPreviousRoomHeight / 2.0f) - (fCurrentRoomHeight / 2.0f)));

                // Sets entry door position = exit door from previous room
                // Vector2 tempDoorPos;
                v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x;
                v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y + (tempRoom.GetRoomHeight() / 2);
                tempRoom.SetRoomEntryDoorPosition(v2tempDoorPos, GameManager.Direction.Up);

                iArrayWorldMapGenY += 1; // If room is down adds one to array X pos
                arrayWorldMap[iArrayWorldMapGenX, iArrayWorldMapGenY] = 1;

                // Gets exitdoor position randomly
                exitDorPos = GetRandomExitDoorPosition(arrayWorldMap, iArrayWorldMapGenX, iArrayWorldMapGenY);
                Debug.LogWarning("Exit door: " + exitDorPos);

                switch (exitDorPos)
                {
                    case GameManager.Direction.Right:    // Door right
                        {
                            v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x + (tempRoom.GetRoomWidth() / 2);
                            v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y;
                            tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Right);
                        }

                        break;
                    case GameManager.Direction.Down: // Door down
                        {
                            v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x;
                            v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y - (tempRoom.GetRoomHeight() / 2);
                            tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Down);
                        }

                        break;
                    case GameManager.Direction.Left: // Door left
                        {
                            v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x - (tempRoom.GetRoomWidth() / 2);
                            v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y;
                            tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Left);
                        }

                        break;
                }
            }

            // New room is right
            if (previousRoomExitDoorDirection == GameManager.Direction.Right)
            {
                // Debug.Log("Room Right");

                // Gets previous room origin and width to calculate new room origin and door positions
                Vector2 previousRoomOrg = lRooms[i].GetRoomOriginPosition();    // Previous room origin
                int iPreviousRoomWidth = lRooms[i].GetRoomWidth();             // Previous room width
                int iCurrentRoomWidth = tempRoom.GetRoomWidth();       // Currently created room width

                // Gets the previous height so that the new room has no more than it; to avoid small corner overlaps
                tempRoom.SetRoomHeight(lRooms[i].GetRoomHeight());

                // Sets up new room origin
                // If current room is Right, new origin is equal to the last origin plus half the width of the previous and current room
                tempRoom.SetRoomOriginPosition(new Vector2(previousRoomOrg.x + (iPreviousRoomWidth / 2.0f) + (iCurrentRoomWidth / 2.0f), previousRoomOrg.y));

                // Sets entry door position = exit door from previous room
                // Vector2 tempDoorPos;
                v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x - (tempRoom.GetRoomWidth() / 2);
                v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y;
                tempRoom.SetRoomEntryDoorPosition(v2tempDoorPos, GameManager.Direction.Left);

                iArrayWorldMapGenX += 1; // If room is right adds one to array Y pos
                arrayWorldMap[iArrayWorldMapGenX, iArrayWorldMapGenY] = 1;

                // Gets exitdoor position randomly
                exitDorPos = GetRandomExitDoorPosition(arrayWorldMap, iArrayWorldMapGenX, iArrayWorldMapGenY);
                Debug.LogWarning("Exit door: " + exitDorPos);

                switch (exitDorPos)
                {
                    case GameManager.Direction.Right:    // Door right
                        {
                            v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x + (tempRoom.GetRoomWidth() / 2);
                            v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y;
                            tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Right);
                        }

                        break;
                    case GameManager.Direction.Down: // Door down
                        {
                            v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x;
                            v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y - (tempRoom.GetRoomHeight() / 2);
                            tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Down);
                        }

                        break;
                    case GameManager.Direction.Up: // Door up
                        {
                            v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x;
                            v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y + (tempRoom.GetRoomHeight() / 2);
                            tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Up);
                        }

                        break;
                }
            }

            // New room is left
            if (previousRoomExitDoorDirection == GameManager.Direction.Left)
            {
                // Debug.Log("Room Left");

                // Gets previous room origin and width to calculate new room origin and door positions
                Vector2 previousRoomOrg = lRooms[i].GetRoomOriginPosition();    // Previous room origin
                int iPreviousRoomWidth = lRooms[i].GetRoomWidth();             // Previous room width
                int iCurrentRoomWidth = tempRoom.GetRoomWidth();       // Currently created room width

                // Gets the previous height so that the new room has no more than it; to avoid small corner overlaps
                tempRoom.SetRoomHeight(lRooms[i].GetRoomHeight());

                // Sets up new room origin
                // If current room is Left, new origin is equal to the last origin minus half the width of the previous and current room
                tempRoom.SetRoomOriginPosition(new Vector2(previousRoomOrg.x - (iPreviousRoomWidth / 2.0f) - (iCurrentRoomWidth / 2.0f), previousRoomOrg.y));

                // Sets entry door position = exit door from previous room
                // Vector2 tempDoorPos;
                v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x + (tempRoom.GetRoomWidth() / 2);
                v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y;
                tempRoom.SetRoomEntryDoorPosition(v2tempDoorPos, GameManager.Direction.Right);

                iArrayWorldMapGenX -= 1; // If room is left sub one to array Y pos
                arrayWorldMap[iArrayWorldMapGenX, iArrayWorldMapGenY] = 1;

                // Gets exitdoor position randomly
                exitDorPos = GetRandomExitDoorPosition(arrayWorldMap, iArrayWorldMapGenX, iArrayWorldMapGenY);
                Debug.LogWarning("Exit door: " + exitDorPos);

                switch (exitDorPos)
                {
                    case GameManager.Direction.Left: // Door left
                        {
                            v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x - (tempRoom.GetRoomWidth() / 2);
                            v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y;
                            tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Left);
                        }

                        break;
                    case GameManager.Direction.Down: // Door down
                        {
                            v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x;
                            v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y - (tempRoom.GetRoomHeight() / 2);
                            tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Down);
                        }

                        break;
                    case GameManager.Direction.Up: // Door up
                        {
                            v2tempDoorPos.x = tempRoom.GetRoomOriginPosition().x;
                            v2tempDoorPos.y = tempRoom.GetRoomOriginPosition().y + (tempRoom.GetRoomHeight() / 2);
                            tempRoom.SetRoomExitDoorPosition(v2tempDoorPos, GameManager.Direction.Up);
                        }

                        break;
                }
            }

            lRooms.Add(tempRoom);

            // For debug
            // PrintArrayWorldMap(arrayWorldMap);
        }

        // Debug
        // DebuggingTools.PrintArrayMatrix(arrayWorldMap);

        // Last room will have no exit door
        lRooms[lRooms.Count - 1].SetRoomExitDoorDirection(GameManager.Direction.None);
    }
    #endregion

    #region Getters
    /// <summary>
    /// Room list getter, to control external access.
    /// </summary>
    /// <returns>List of generated rooms.</returns>
    public List<Room> GetRoomsList()
    {
        return lRooms;
    }

    /// <summary>
    /// Current world map matrix getter, to control external access.
    /// </summary>
    /// <returns>World map matrix.</returns>
    public int[,] GetarrayWorldMap()
    {
        return arrayWorldMap;
    }
    #endregion

    /// <summary>
    /// Calculates and returns a random generated exit door position, taking into account the existing adjacent rooms to not overlap.
    /// </summary>
    /// <param name="currentarrayWorldMap">Current existing array map.</param>
    /// <param name="currentGenX">Current X position of array map.</param>
    /// <param name="currentGenY">Current Y position of array map.</param>
    /// <returns>Direction of exit door.</returns>
    public GameManager.Direction GetRandomExitDoorPosition(int[,] currentarrayWorldMap, int currentGenX, int currentGenY)
    {
        // Variable to store valid positions (empty positions in the arrayWorldMap)
        List<GameManager.Direction> validOptions = new List<GameManager.Direction>();

        // Room empty to the right (and adjacent, to avoid small overlaps)
        if (currentarrayWorldMap[currentGenX + 1, currentGenY] == 0 && currentarrayWorldMap[currentGenX + 2, currentGenY] == 0 && currentarrayWorldMap[currentGenX + 1, currentGenY - 1] == 0 && currentarrayWorldMap[currentGenX + 1, currentGenY + 1] == 0)
        {
            validOptions.Add(GameManager.Direction.Right);
        }

        // Room empty to the left
        if (currentarrayWorldMap[currentGenX - 1, currentGenY] == 0 && currentarrayWorldMap[currentGenX - 2, currentGenY] == 0 && currentarrayWorldMap[currentGenX - 1, currentGenY - 1] == 0 && currentarrayWorldMap[currentGenX - 1, currentGenY + 1] == 0)
        {
            validOptions.Add(GameManager.Direction.Left);
        }

        // Room empty up
        if (currentarrayWorldMap[currentGenX, currentGenY - 1] == 0 && currentarrayWorldMap[currentGenX, currentGenY - 2] == 0 && currentarrayWorldMap[currentGenX + 1, currentGenY - 1] == 0 && currentarrayWorldMap[currentGenX - 1, currentGenY - 1] == 0)
        {
            validOptions.Add(GameManager.Direction.Up);
        }

        // Room empty down
        if (currentarrayWorldMap[currentGenX, currentGenY + 1] == 0 && currentarrayWorldMap[currentGenX, currentGenY + 2] == 0 && currentarrayWorldMap[currentGenX + 1, currentGenY + 1] == 0 && currentarrayWorldMap[currentGenX - 1, currentGenY + 1] == 0)
        {
            validOptions.Add(GameManager.Direction.Down);
        }

        Debug.LogWarning(validOptions.Count);
        return validOptions[Random.Range(0, validOptions.Count)];
    }
}
