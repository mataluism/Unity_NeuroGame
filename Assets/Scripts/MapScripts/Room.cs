﻿//-----------------------------------------------------------------------
// <copyright file="Room.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-21, 12:29</last_modified>
// <summary>Room random dimensions generation class.</summary>
//-----------------------------------------------------------------------
using UnityEngine;

/// <summary>
/// Random room dimensions generation.
/// </summary>
public class Room
{
    /// <summary>
    /// Min width of the room.
    /// </summary>
    private static int iRoomMinWidth = 10;

    /// <summary>
    /// Min height of the room.
    /// </summary>
    private static int iRoomMinHeight = iRoomMinWidth;

    /// <summary>
    /// Max width of the room.
    /// </summary>
    private static int iRoomMaxWidth = (iRoomMinWidth * 3) / 2;

    /// <summary>
    /// Max height of the room.
    /// </summary>
    private static int iRoomMaxHeight = (iRoomMinWidth * 3) / 2;

    /// <summary>
    /// Number of doors per room.
    /// </summary>
    private static int iRoomNumDoors = 2;

    /// <summary>
    /// Generated room origin.
    /// </summary>
    private Vector2 v2RoomOriginPosition;

    /// <summary>
    /// Generated room width.
    /// </summary>
    private int iRoomWidth;

    /// <summary>
    /// Generated room height.
    /// </summary>
    private int iRoomHeight;

    /// <summary>
    /// Generated room exit door position.
    /// </summary>
    private Vector2 v2RoomExitDoorPosition;

    /// <summary>
    /// Generated room entry door position.
    /// </summary>
    private Vector2 v2RoomEntryDoorPosition;

    /// <summary>
    /// Generated room exit door direction.
    /// </summary>
    private GameManager.Direction exitDoorDirection;

    /// <summary>
    /// Generated room entry door direction.
    /// </summary>
    private GameManager.Direction entryDoorDirection;

    #region Constructors
    /// <summary>
    /// Initializes a new instance of the <see cref="Room"/> class. Used to randomly generate room sizes.
    /// </summary>
    public Room()
    {
        // Random generate width and height; cast to int to avoid dec(at this point they are int either way)
        iRoomWidth = Random.Range(iRoomMinWidth, iRoomMaxWidth + 1);
        iRoomHeight = Random.Range(iRoomMinHeight, iRoomMaxHeight + 1);

        // By default the entry door is empty/none 
        v2RoomEntryDoorPosition.x = 0;
        v2RoomEntryDoorPosition.y = 0;
        entryDoorDirection = GameManager.Direction.None;

        // By default the entry door is at the bottom of the map
        /*v2RoomEntryDoorPosition.x = 0;
        v2RoomEntryDoorPosition.y = -fRoomHeight / 2;
        EntryDoorDirection = GameManager.Direction.Down;*/

        // By default the exit door is at the top of the map
        v2RoomExitDoorPosition.x = 0;
        v2RoomExitDoorPosition.y = iRoomHeight / 2;
        exitDoorDirection = GameManager.Direction.Up;

        // By default the room is created in the center of the World
        v2RoomOriginPosition.x = 0;
        v2RoomOriginPosition.y = 0;
    }
    #endregion

    #region Setters
    /// <summary>
    /// Sets room entry door position.
    /// </summary>
    /// <param name="v2NewDoorPos">World coordinates position.</param>
    /// <param name="newDir">Door placement direction.</param>
    public void SetRoomEntryDoorPosition(Vector2 v2NewDoorPos, GameManager.Direction newDir)
    {
        v2RoomEntryDoorPosition = v2NewDoorPos;
        entryDoorDirection = newDir;
    }

    /// <summary>
    /// Sets room exit door position.
    /// </summary>
    /// <param name="v2NewDoorPos">World coordinates position.</param>
    /// <param name="newDir">Door placement direction.</param>
    public void SetRoomExitDoorPosition(Vector2 v2NewDoorPos, GameManager.Direction newDir)
    {
        v2RoomExitDoorPosition = v2NewDoorPos;
        exitDoorDirection = newDir;
    }

    /// <summary>
    /// Sets room origin position.
    /// </summary>
    /// <param name="v2NewRoomOrig">World coordinates room origin position.</param>
    public void SetRoomOriginPosition(Vector2 v2NewRoomOrig)
    {
        v2RoomOriginPosition = v2NewRoomOrig;
    }

    /// <summary>
    /// Sets room exit door direction.
    /// </summary>
    /// <param name="newDir">Exit door placement direction.</param>
    public void SetRoomExitDoorDirection(GameManager.Direction newDir)
    {
        exitDoorDirection = newDir;
    }

    /// <summary>
    /// Sets room width.
    /// </summary>
    /// <param name="newRoomWidth">New room width.</param>
    public void SetRoomWidth(int newRoomWidth)
    {
        iRoomWidth = newRoomWidth;
    }

    /// <summary>
    /// Sets room width.
    /// </summary>
    /// <param name="newRoomHeight">New room height.</param>
    public void SetRoomHeight(int newRoomHeight)
    {
        iRoomHeight = newRoomHeight;
    }
    #endregion

    #region Getters
    /// <summary>
    /// Gets room width.
    /// </summary>
    /// <returns>Room width.</returns>
    public int GetRoomWidth()
    {
        return iRoomWidth;
    }

    /// <summary>
    /// Gets room height.
    /// </summary>
    /// <returns>Room height.</returns>
    public int GetRoomHeight()
    {
        return iRoomHeight;
    }

    /// <summary>
    /// Gets room num doors.
    /// </summary>
    /// <returns>Room num doors.</returns>
    public int GetRoomNumDoors()
    {
        return iRoomNumDoors;
    }

    /// <summary>
    /// Gets room entry door world coordinates.
    /// </summary>
    /// <returns>Entry door position.</returns>
    public Vector2 GetRoomEntryDoorPosition()
    {
        return v2RoomEntryDoorPosition;
    }

    /// <summary>
    /// Gets room entry door direction.
    /// </summary>
    /// <returns>Entry door direction.</returns>
    public GameManager.Direction GetRoomEntryDoorDirection()
    {
        return entryDoorDirection;
    }

    /// <summary>
    /// Gets room exit door world coordinates.
    /// </summary>
    /// <returns>Exit door position.</returns>
    public Vector2 GetRoomExitDoorPosition()
    {
        return v2RoomExitDoorPosition;
    }

    /// <summary>
    /// Gets room exit door direction.
    /// </summary>
    /// <returns>Exit door direction.</returns>
    public GameManager.Direction GetRoomExitDoorDirection()
    {
        return exitDoorDirection;
    }

    /// <summary>
    /// Gets room origin coordinates.
    /// </summary>
    /// <returns>Room origin position.</returns>
    public Vector2 GetRoomOriginPosition()
    {
        return v2RoomOriginPosition;
    }

    /// <summary>
    /// Gets room max width set.
    /// </summary>
    /// <returns>Room max width.</returns>
    public int GetRoomsMaxWidth()
    {
        return iRoomMaxWidth;
    }

    /// <summary>
    /// Gets room max height set.
    /// </summary>
    /// <returns>Room max height.</returns>
    public int GetRoomMaxHeight()
    {
        return iRoomMaxHeight;
    }
    #endregion
}
