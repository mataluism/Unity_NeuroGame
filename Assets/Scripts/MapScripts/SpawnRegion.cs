﻿////-----------------------------------------------------------------------
//// <copyright file="SpawnRegion.cs" company="Luís Mata">
////		Copyright (c) 2016 Luís Mata.
//// </copyright>
//// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
//// <author>Luís Mata</author>
//// <last_modified>2016-8-21, 12:59</last_modified>
//// <summary>Enemy spawn region definition.</summary>
////-----------------------------------------------------------------------
//using System.Collections;
//using UnityEngine;

///// <summary>
///// Spawn region values.
///// </summary>
//public class SpawnRegion
//{
//    /// <summary>
//    /// World current position.
//    /// </summary>
//    private Vector2 v2Position;

//    /// <summary>
//    /// Spawn region width.
//    /// </summary>
//    private float fRegionWidth;

//    /// <summary>
//    /// Spawn region height.
//    /// </summary>
//    private float fRegionHeight;

//    /// <summary>
//    /// Spawn region cardinal point (direction).
//    /// </summary>
//    private GameManager.Direction iCardinalPoint;

//    #region Constructors
//    /// <summary>
//    /// Initializes a new instance of the <see cref="SpawnRegion"/> class. Used to calcaulate possible spawn regions outside camera.
//    /// </summary>
//    public SpawnRegion()
//    {
//        v2Position.x = 0;
//        v2Position.y = 0;
//        fRegionWidth = 0;
//        fRegionHeight = 0;
//    }

//    /// <summary>
//    /// Initializes a new instance of the <see cref="SpawnRegion"/> class. Used to calcaulate possible spawn regions outside camera.
//    /// </summary>
//    /// <param name="v2Pos">Current position.</param>
//    /// <param name="fWid">Region width.</param>
//    /// <param name="fHei">Region height.</param>
//	public SpawnRegion(Vector2 v2Pos, float fWid, float fHei)
//    {
//        v2Position.x = v2Pos.x;
//        v2Position.y = v2Pos.y;
//        fRegionWidth = fWid;
//        fRegionHeight = fHei;
//    }
//    #endregion

//    #region Setters
//    /// <summary>
//    /// Set current position.
//    /// </summary>
//    /// <param name="fPosX">X value position.</param>
//    /// <param name="fPosY">Y value position.</param>
//    public void SetPosition(float fPosX, float fPosY)
//    {
//        v2Position.x = fPosX;
//        v2Position.y = fPosY;
//    }

//    /// <summary>
//    /// Set region width.
//    /// </summary>
//    /// <param name="fWid">New region width.</param>
//	public void SetWidth(float fWid)
//    {
//        fRegionWidth = fWid;
//    }

//    /// <summary>
//    /// Set region height.
//    /// </summary>
//    /// <param name="fHei">New region height.</param>
//	public void SetHeight(float fHei)
//    {
//        fRegionHeight = fHei;
//    }

//    /// <summary>
//    /// Set region cardinal point.
//    /// </summary>
//    /// <param name="iCard">New region cardinal point.</param>
//	public void SetCardinalPoint(GameManager.Direction iCard)
//    {
//        iCardinalPoint = iCard;
//    }
//    #endregion

//    #region Getters
//    /// <summary>
//    /// Get spawn region position.
//    /// </summary>
//    /// <returns>Region position.</returns>
//    public Vector2 GetPosition()
//    {
//        return v2Position;
//    }

//    /// <summary>
//    /// Get region width.
//    /// </summary>
//    /// <returns>Spawn region width.</returns>
//    public float GetWidth()
//    {
//        return fRegionWidth;
//    }

//    /// <summary>
//    /// Get region height.
//    /// </summary>
//    /// <returns>Spawn region height.</returns>
//    public float GetHeight()
//    {
//        return fRegionHeight;
//    }

//    /// <summary>
//    /// Get spawn region cardinal point.
//    /// </summary>
//    /// <returns>Spawn region cardinal point.</returns>
//    public GameManager.Direction GetCardinalPoint()
//    {
//        return iCardinalPoint;
//    }

//    /// <summary>
//    /// Spawn region area.
//    /// </summary>
//    /// <returns> Calculted spawn region area.</returns>
//    public float GetRegionArea()
//    {
//        return GetHeight() * GetWidth();
//    }
//    #endregion
//}
