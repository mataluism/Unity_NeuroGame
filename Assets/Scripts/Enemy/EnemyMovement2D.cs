﻿//-----------------------------------------------------------------------
// <copyright file="EnemyMovement2D.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-20, 12:27</last_modified>
// <summary>Enemy movement.</summary>
//-----------------------------------------------------------------------
using System.Collections;
using UnityEngine;

/// <summary>
/// Enemy chasing movement.
/// </summary>
public class EnemyMovement2D : MonoBehaviour
{
    /// <summary>
    /// Internal variable to control debug log.
    /// </summary>
    private bool bIsDebugEnemyMovement = false;

    /// <summary>
    /// Stores enemy stats from the Script EnemyStatsnSettings.
    /// </summary>
	EnemyStatsnSettings enemyStatsScript;

    /// <summary>
    /// Stores the player transform.
    /// </summary>
	GameObject goPlayer;

    /// <summary>
    /// Current player position.
    /// </summary>
	Vector2 v2PlayerPosition;

    /// <summary>
    /// Current (this) enemy position.
    /// </summary>
	Vector2 v2CurrentPosition;

    /// <summary>
    /// Distance between this and the player object.
    /// </summary>
	float fDistance;

    /// <summary>
    /// Enemy movement vector.
    /// </summary>
	Vector2 v2EnemyMove;

    /// <summary>
    /// Animation component.
    /// </summary>
    Animator animEnemy;

    /// <summary>
    /// Variable to control where the enemy is turned.
    /// </summary>
	GameManager.Direction iFacingDirection;

    /// <summary>
    /// If enemy is colliding with character.
    /// </summary>
    bool bIsCollidingCharacter;

    #region Animation type
    /// <summary>
    /// Stopwatch to control animation type change.
    /// </summary>
    private System.Diagnostics.Stopwatch animEnemyTypeChange;

    /// <summary>
    /// Stopwatch to control animation type wait time.
    /// </summary>
    private System.Diagnostics.Stopwatch animEnemyTypeChangeInterval;

    /// <summary>
    /// If animation selected is Magic.
    /// </summary>
    private bool bIsAnimEnemyMagic;

    /// <summary>
    /// Max type change duration in seconds.
    /// </summary>
    private int iEnemyTypeChangeMaxDuration = 2;

    /// <summary>
    /// Min type change duration in seconds.
    /// </summary>
    private int iEnemyTypeChangeMinDuration = 1;

    /// <summary>
    /// Max interval until anim type changes seconds.
    /// </summary>
    private int iEnemyTypeMaxInterval = 10;

    /// <summary>
    /// Min interval until anim type changes seconds.
    /// </summary>
    private int iEnemyTypeMinInterval = 7;

    /// <summary>
    /// Random value of next type change duration.
    /// </summary>
    private int iAnimEnemyTypeChangeDuration;

    /// <summary>
    /// Random value of interval until anim type changes.
    /// </summary>
    private int iAnimEnemyTypeInterval;
    #endregion

    #region Animation speed
    /// <summary>
    /// Stopwatch to control animation speed change.
    /// </summary>
    private System.Diagnostics.Stopwatch animEnemySpeedChange;

    /// <summary>
    /// Max speed animation can have.
    /// </summary>
    private float fAnimEnemyMaxSpeed = 1.5f;

    /// <summary>
    /// Min speed animation can have.
    /// </summary>
    private float fAnimEnemyMinSpeed = 0.2f;

    /// <summary>
    /// Animation normal speed.
    /// </summary>
    private float fAnimEnemyNormalSpeed = 1.0f;

    /// <summary>
    /// Max speed change duration in seconds.
    /// </summary>
    private int iEnemySpeedChangeMaxDuration = 5;

    /// <summary>
    /// Min speed change duration in seconds.
    /// </summary>
    private int iEnemySpeedChangeMinDuration = 2;

    /// <summary>
    /// Random value of next speed change duration.
    /// </summary>
    private int iAnimEnemySpeedChangeDuration;

    /// <summary>
    /// If anim speed is changed.
    /// </summary>
    private bool bIsAnimEnemySpeedChanged;
    #endregion

    /// <summary>
    /// Enemy dying trigger. Called by the last fram of enemy dying animation.
    /// </summary>
    public void FinishedDying()
    {
        CharStatsnSettings.bIsAttackingLocked = false;

        // Destroys enemy after finishing dying animation plus 1 second on the floor
        Destroy(gameObject, 1f);
    }

    /// <summary>
    /// Inherited from MonoBehaviour. Use this for initialization.
    /// </summary>
    void Start()
    {
        enemyStatsScript = GetComponent<EnemyStatsnSettings>(); // Gets script to access stats
        goPlayer = GameObject.FindGameObjectWithTag("Player");		// Finds and stores player gameobject

        bIsCollidingCharacter = false;

        animEnemy = GetComponent<Animator>();

        // Initializes speed change variables
        animEnemySpeedChange = new System.Diagnostics.Stopwatch();
        iAnimEnemySpeedChangeDuration = 0;
        bIsAnimEnemySpeedChanged = false;

        // Initializes anim type change variables
        animEnemyTypeChange = new System.Diagnostics.Stopwatch();
        animEnemyTypeChangeInterval = new System.Diagnostics.Stopwatch();
        iAnimEnemyTypeChangeDuration = 0;
        iAnimEnemyTypeInterval = 0;
        bIsAnimEnemyMagic = false;
        animEnemy.SetBool("bIsMagic", bIsAnimEnemyMagic);

        Random.seed = (int)System.DateTime.Now.Ticks;

        // Set first wait time for type change and starts stopwatch
        iAnimEnemyTypeInterval = Random.Range(iEnemyTypeMinInterval, iEnemyTypeMaxInterval + 1);
        animEnemyTypeChangeInterval = System.Diagnostics.Stopwatch.StartNew();

    }

    /// <summary>
    /// Inherited from MonoBehaviour. Update is called once per frame.
    /// </summary>
    void Update()
    {
        // Gets position of player and enemy and calculates the vector between them
        v2PlayerPosition = goPlayer.transform.position;
        v2CurrentPosition = transform.position;
        fDistance = Vector2.Distance(v2PlayerPosition, v2CurrentPosition);

        // If enemy can "see" player and is not locked in a fight
        if (!bIsCollidingCharacter && fDistance < enemyStatsScript.fSightDistance && !CharStatsnSettings.bIsAttackingLocked)
        {
            v2EnemyMove = v2PlayerPosition - v2CurrentPosition;

            // Normalizes result and adds the correct speed
            v2EnemyMove = v2EnemyMove.normalized * enemyStatsScript.fSpeed;
        }
        else
        {
            // Enemy has no reason to move
            v2EnemyMove.Set(0, 0);
        }

        if (enemyStatsScript.bIsDying)
        {
            // starts dying animation
            animEnemy.SetBool("bIsDying", true);
            animEnemy.speed = fAnimEnemyNormalSpeed;

        }

        iFacingDirection = GameObjectExtensions.GetFacingDirectionMovement(v2EnemyMove); // Gets enemy movement and calculates facing direction
        animEnemy.SetBool("bIsStopped", GameObjectExtensions.CheckIfStopped(v2EnemyMove.normalized));            // Set speed parameter for idle/walk animation
        animEnemy.SetInteger("iFacingDirection", (int)iFacingDirection);      // Plays correct animation

        if(bIsCollidingCharacter)
        {
            ChangeAnimEnemy();
        }
    }

    /// <summary>
    /// Changes and controls enemy animations.
    /// </summary>
    private void ChangeAnimEnemy()
    {
        // if speed is normal
        if (!bIsAnimEnemySpeedChanged)
        {
            // Randomly gets speed change duration
            iAnimEnemySpeedChangeDuration = UnityEngine.Random.Range(iEnemySpeedChangeMinDuration, iEnemySpeedChangeMaxDuration + 1);

            // Random anim speed 
            animEnemy.speed = UnityEngine.Random.Range(fAnimEnemyMinSpeed, fAnimEnemyMaxSpeed);

            if (bIsDebugEnemyMovement)
            {
                Debug.Log("not bIsAnimEnemySpeedChanged");
                Debug.Log("iAnimEnemySpeedChangeDuration: " + iAnimEnemySpeedChangeDuration + " | AnimEnemy speed set: " + animEnemy.speed);
            }

            // Starts speed change control stopwatch
            animEnemySpeedChange = System.Diagnostics.Stopwatch.StartNew();

            bIsAnimEnemySpeedChanged = true;
        }
        else if (bIsAnimEnemySpeedChanged && animEnemySpeedChange.ElapsedMilliseconds > iAnimEnemySpeedChangeDuration * 1000)
        {
            // If animation is set to different speed and the speed change duration has passed restores states
            animEnemy.speed = fAnimEnemyNormalSpeed;
            animEnemySpeedChange.Stop();

            if (bIsDebugEnemyMovement)
            {
                Debug.Log("bIsAnimEnemySpeedChanged = true");
                Debug.Log("iAnimEnemySpeedChangeDuration: " + iAnimEnemySpeedChangeDuration + " | AnimEnemy speed set: " + animEnemy.speed);
            }
            bIsAnimEnemySpeedChanged = false;
        }

        // Anim type
        // If magic animation is not active and wait time is ellapsed
        if (!bIsAnimEnemyMagic && animEnemyTypeChangeInterval.ElapsedMilliseconds > iAnimEnemyTypeInterval * 1000)
        {
            // Randomly gets type change duration
            iAnimEnemyTypeChangeDuration = UnityEngine.Random.Range(iEnemyTypeChangeMinDuration, iEnemyTypeChangeMaxDuration + 1);

            if (bIsDebugEnemyMovement)
            {
                Debug.Log("not bIsAnimMagic");
                Debug.Log("iAnimTypeChangeDuration: " + iAnimEnemyTypeChangeDuration + " | Anim bIsMagic set: " + animEnemy.GetBool("bIsMagic"));
            }

            // Starts type change control stopwatch
            animEnemyTypeChange = System.Diagnostics.Stopwatch.StartNew();

            bIsAnimEnemyMagic = true;

            // Sets animation to magic
            animEnemy.SetBool("bIsMagic", bIsAnimEnemyMagic);
        }
        else if (bIsAnimEnemyMagic && animEnemyTypeChange.ElapsedMilliseconds > iAnimEnemyTypeChangeDuration * 1000)
        {
            bIsAnimEnemyMagic = false;

            // Sets animation to normal attack
            animEnemy.SetBool("bIsMagic", false);
            animEnemyTypeChange.Stop();

            animEnemyTypeChangeInterval.Stop();
            // Set wait time for type change and starts stopwatch
            iAnimEnemyTypeInterval = UnityEngine.Random.Range(iEnemyTypeMinInterval, iEnemyTypeMaxInterval + 1);
            animEnemyTypeChangeInterval = System.Diagnostics.Stopwatch.StartNew();

            if (bIsDebugEnemyMovement)
            {
                Debug.Log("bIsAnimMagic = true");
                Debug.Log("iAnimTypeChangeDuration: " + iAnimEnemyTypeChangeDuration + " | Anim bIsMagic set: " + animEnemy.GetBool("bIsMagic"));
            }
        }
    }

    /// <summary>
    /// Inherited from MonoBehaviour. FixedUpdate is called every fixed framerate frame.
    /// Used for physics processing.
    /// </summary>
    void FixedUpdate()
    {
        GetComponent<Rigidbody2D>().velocity = v2EnemyMove;
    }

    /// <summary>
    /// Inherited from MonoBehaviour. OnCollision enter event.
    /// </summary>
    /// <param name="c2Collision">Collision that activated the event.</param>
    void OnCollisionEnter2D(Collision2D c2Collision)
    {
        GameObject goObjCollided = c2Collision.gameObject;

        if (goObjCollided.CompareTag("Player"))
        {
            v2EnemyMove = Vector2.zero;
            bIsCollidingCharacter = true;   // Collision set to true to stop enemy movement and intiate fight procedure
            iFacingDirection = GameObjectExtensions.GetCharaterDirection(this.gameObject, goPlayer);
            animEnemy.SetInteger("iFacingDirection", (int)iFacingDirection);      // Plays correct animation
            animEnemy.SetBool("bIsAttacking", true);
        }
    }

    /// <summary>
    /// Inherited from MonoBehaviour. Executed when object destruction is called.
    /// </summary>
    void OnDestroy()
    {
        // Sets the spawner ready for next enemy
        EnemySpawner.bIsEnemyAlive = false;
    }
}
