﻿//-----------------------------------------------------------------------
// <copyright file="EnemySpawner.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-20, 12:43</last_modified>
// <summary>Enemy Spawner.</summary>
//-----------------------------------------------------------------------
using System;
using UnityEngine;

/// <summary>
/// Enemy spawn script.
/// </summary>
public class EnemySpawner : MonoBehaviour
{
    /// <summary>
    /// If enemy is alive to control next spawn.
    /// </summary>
    public static bool bIsEnemyAlive;

    /// <summary>
    /// Enemy prefab to spawn.
    /// </summary>
    private GameObject goEnemyPrefab;

    /// <summary>
    /// Enemy GameObject spawned.
    /// </summary>
    private GameObject goEnemy;

    /// <summary>
    /// Existing map.
    /// </summary>
    MapDrawer mapDrawn;

    /// <summary>
    /// Number of rooms available to spawn enemies.
    /// </summary>
    private int iRoomsToSpawn;

    /// <summary>
    /// Number of enemies already spawned. used to control remaining enemies.
    /// </summary>
    private int iTotalEnemiesSpawned;

    /// <summary>
    /// Enemy spawn position.
    /// </summary>
    private Vector2 v2SpawnPosition;

    /// <summary>
    /// If map is ready to start spawning.
    /// </summary>
    private bool bStartSpawning;

    /// <summary>
    /// Finished. No more enemies to spawn dialog only once.
    /// </summary>
    private bool bFinishedGameShownOnce;

    /// <summary>
    /// Extended method to add the component, controlling its order.
    /// </summary>
    /// <param name="objectAddingEnemySpawnerTo">Object to add enemy spawn.</param>
    /// <param name="passedDrawnMap">Current drawn map.</param>
    /// <returns>EnemySpawner component.</returns>
    public static EnemySpawner AddEnemySpawnerComponent(GameObject objectAddingEnemySpawnerTo, MapDrawer passedDrawnMap)
    {
        // Deactivates the object to avoid immediately running MonoBehaviour functions
        objectAddingEnemySpawnerTo.SetActive(false);

        // Adds the component
        EnemySpawner enemySp = objectAddingEnemySpawnerTo.AddComponent<EnemySpawner>() as EnemySpawner;

        // Sets the reference to the "calling" class
        enemySp.mapDrawn = passedDrawnMap;

        // Activates the object allowing its execution
        objectAddingEnemySpawnerTo.SetActive(true);
        return enemySp;
    }

    /// <summary>
    /// Inherited from MonoBehaviour. Use this for initialization.
    /// </summary>
    void Start()
    {
        bStartSpawning = false;
        bFinishedGameShownOnce = false;

        // Enemy Spawning
        // Gets the prefabs and variables ready
        goEnemyPrefab = Resources.Load<GameObject>("Prefabs/Characters/Enemy1");

        // Gets the total number of rooms to spawn. -1 since first room doesn't have
        iRoomsToSpawn = mapDrawn.lRoom.Count - 1;
        iTotalEnemiesSpawned = 0;
        v2SpawnPosition = Vector2.zero;
        bIsEnemyAlive = false;              // Controls that only one enemy exist at a time
        Debug.Log("EnemySpawner Started");
    }

    /// <summary>
    /// Inherited from MonoBehaviour. Update is called once per frame.
    /// </summary>
    void Update()
    {
        if (iTotalEnemiesSpawned == iRoomsToSpawn)
        {
            bStartSpawning = false;
        }

        if(!bStartSpawning && !bIsEnemyAlive && iTotalEnemiesSpawned == iRoomsToSpawn && !bFinishedGameShownOnce)
        {
            MenuUI.ShowSingleDialogByIndex(47);
            bFinishedGameShownOnce = true;
        }

        if (!bStartSpawning && iTotalEnemiesSpawned < iRoomsToSpawn)
        {
            bStartSpawning = true;
            //// iRoomsToSpawn = mapDrawn.lRoom.Count; //variable to keep track of the number of enemies to spawn
        }

        // If spawn is on and no enemy is alive and there are still enemies to spawn
        if (bStartSpawning && !bIsEnemyAlive && iTotalEnemiesSpawned < iRoomsToSpawn)
        {
            goEnemy = SpawnAnEnemy();
        }
    }

    /// <summary>
    /// Spawns enemy with set variables.
    /// </summary>
    /// <returns>Enemy spawned GameObject.</returns>
    GameObject SpawnAnEnemy()
    {
        // Debug.Log("Going to spawn an enemy");
        v2SpawnPosition = mapDrawn.lRoom[iTotalEnemiesSpawned + 1].GetRoomOriginPosition();

        // Sets spawn position to z 1 to get below player
        goEnemy = Instantiate(goEnemyPrefab, new Vector3(v2SpawnPosition.x, v2SpawnPosition.y, 1), transform.rotation) as GameObject;

        // changes enemy to editor hierarchy
        goEnemy.transform.parent = GameObject.Find("Foreground").transform;

        MenuUI.tEnemiesRemainingText.text = (iRoomsToSpawn - iTotalEnemiesSpawned).ToString();

        iTotalEnemiesSpawned++;

        // For the new enemy health, it is considered both the current enemy and the current difficulty
        double dNewEnemyHealth = (Math.Pow((goEnemy.GetComponent<EnemyStatsnSettings>().GetdBaseHealth() * iTotalEnemiesSpawned * DynamicDifficulty.GetCurrentDifficultyLevel()), DynamicDifficulty.dPowerValueForCalculations)) / DynamicDifficulty.dEnemyHealthDivisor;

        GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": EnemySpawner::SpawnAnEnemy() - Spawned new enemy with health =" + dNewEnemyHealth + ". Total enemies spawned =" + iTotalEnemiesSpawned);

        // The new enemy will have its health multiplied by the number of enemies already spawn
        goEnemy.GetComponent<EnemyStatsnSettings>().SetdCurrentHealth(dNewEnemyHealth);

        // Sets current player power
        CharStatsnSettings.dCurrentAtkPower = CharStatsnSettings.dBaseAtkPower * DynamicDifficulty.GetCurrentDifficultyLevel() * DynamicDifficulty.dPowerValueForCalculations;

        bIsEnemyAlive = true;
        return goEnemy;
    }
}
