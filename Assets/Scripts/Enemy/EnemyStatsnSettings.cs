﻿//-----------------------------------------------------------------------
// <copyright file="EnemyStatsnSettings.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-20, 12:54</last_modified>
// <summary>Enemy base stats.</summary>
//-----------------------------------------------------------------------
using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Defines character base stats and user settings.
/// </summary>
public class EnemyStatsnSettings : MonoBehaviour
{
    /// <summary>
    /// If enemy is close and can be hit.
    /// </summary>
    public bool bCanBeHit;

    /// <summary>
    /// Enemy's initial Movement speed.
    /// </summary>              
    public float fSpeed = 1;
    //// public float fAtkPower = 10; // Enemy's initial Attack Power

    /// <summary>
    /// How far can the enemy see the player.
    /// </summary>              
    public float fSightDistance = 4;

    /// <summary>
    /// Enemy variable to play dying procedure.
    /// </summary>
    public bool bIsDying;

    #region HUD Health Instantiation variables
    /// <summary>
    /// GameObject to store the HUD prefab.
    /// </summary>              
    GameObject goHUD;

    /// <summary>
    /// HUD instantiation.
    /// </summary>
    GameObject goUIHealth;

    /// <summary>
    /// Health HUD slider.
    /// </summary>              
    Slider healthSlider;

    /// <summary>
    /// Enemy's initial health.
    /// </summary>
    private double dBaseHealth = 50;

    /// <summary>
    /// Enemy's current health.
    /// </summary>
    private double dCurrentHealth = 50;
    #endregion

    /// <summary>
    /// Get base enemy health value.
    /// </summary>
    /// <returns>Base health of the enemy.</returns>
    public double GetdBaseHealth()
    {
        return dBaseHealth;
    }

    /// <summary>
    /// Get current enemy health.
    /// </summary>
    /// <returns>Current health of the enemy.</returns>
    public double GetdCurrentHealth()
    {
        return dCurrentHealth;
    }

    /// <summary>
    /// Set enemy health.
    /// </summary>
    /// <param name="newHealthValue">New enemy health value.</param>
    public void SetdCurrentHealth(double dNewHealthValue)
    {
        dCurrentHealth = dNewHealthValue;
    }

    /// <summary>
    /// Inherited from MonoBehaviour. Use this for initialization.
    /// </summary>
    void Start()
    {
        goHUD = Resources.Load<GameObject>("Prefabs/Characters/EnemyHUD");

        goUIHealth = Instantiate(goHUD) as GameObject;
        healthSlider = GameObject.Find("HUDCanvas/Image/HealthSlider").GetComponent<Slider>();

        // Sets the initial max value for the health slider
        healthSlider.maxValue = (float)dCurrentHealth;

        // When enemy spawns HUD is not visible
        goUIHealth.SetActive(false);

        bIsDying = false;
    }

    /// <summary>
    /// Inherited from MonoBehaviour. Update is called once per frame.
    /// </summary>
    void Update()
    {
        if (bCanBeHit)
        {
            goUIHealth.SetActive(true);
        }

        if (goUIHealth.activeSelf)
        {
            healthSlider.value = (float)dCurrentHealth;
        }

        // Verifies current health
        if (dCurrentHealth <= 0 && !bIsDying)
        {
            GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ". EnemyStatsnSettings::Update() - Enemy died.");

            // disables collider
            gameObject.GetComponent<BoxCollider2D>().enabled = false;

            // Starts dying procedure
            bIsDying = true;
        }
    }

    /// <summary>
    /// Inherited from MonoBehaviour. This function is called when the MonoBehaviour will be destroyed.
    /// When enemy is destroyed, HUD must also be destroyed.
    /// </summary>
    void OnDestroy()
    {
        Destroy(goUIHealth);
    }
}
