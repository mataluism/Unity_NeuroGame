﻿//-----------------------------------------------------------------------
// <copyright file="CharMovement2D.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-20, 12:15</last_modified>
// <summary>Character movement.</summary>
//-----------------------------------------------------------------------
using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Character movement.
/// </summary>
public class CharMovement2D : MonoBehaviour
{
    /// <summary>
    /// Internal variable to control debug log.
    /// </summary>
    private bool bIsDebugCharacterMovement = false;

    /// <summary>
    /// Animation component.
    /// </summary>
    public Animator anim;

    /// <summary>
    /// Movement vector.
    /// </summary>
	Vector2 v2Move;

    /// <summary>
    /// Speed calculation vector, to control the speed according to the direction.
    /// </summary>
	Vector2 v2Speed;

    /// <summary>
    /// Direction vector, to store normalized value.
    /// </summary>
	Vector2 v2Direction;

    /// <summary>
    /// Variable to control where player is turned.
    /// </summary>
	GameManager.Direction iFacingDirection;

    /// <summary>
    /// Current enemy gameobject to set fighting direction.
    /// </summary>
    GameObject goEnemy;

    #region Animation type
    /// <summary>
    /// Stopwatch to control animation type change.
    /// </summary>
    private System.Diagnostics.Stopwatch animTypeChange;

    /// <summary>
    /// Stopwatch to control animation type wait time.
    /// </summary>
    private System.Diagnostics.Stopwatch animTypeChangeInterval;

    /// <summary>
    /// If animation selected is Magic.
    /// </summary>
    private bool bIsAnimMagic;

    /// <summary>
    /// Max type change duration in seconds.
    /// </summary>
    private int iTypeChangeMaxDuration = 2;

    /// <summary>
    /// Min type change duration in seconds.
    /// </summary>
    private int iTypeChangeMinDuration = 1;

    /// <summary>
    /// Max interval until anim type changes seconds.
    /// </summary>
    private int iTypeMaxInterval = 7;

    /// <summary>
    /// Min interval until anim type changes seconds.
    /// </summary>
    private int iTypeMinInterval = 3;

    /// <summary>
    /// Random value of next type change duration.
    /// </summary>
    private int iAnimTypeChangeDuration;

    /// <summary>
    /// Random value of interval until anim type changes.
    /// </summary>
    private int iAnimTypeInterval;
    #endregion

    #region Animation speed
    /// <summary>
    /// Stopwatch to control animation speed change.
    /// </summary>
    private System.Diagnostics.Stopwatch animSpeedChange;

    /// <summary>
    /// Max speed animation can have.
    /// </summary>
    private float fAnimMaxSpeed = 2.0f;

    /// <summary>
    /// Min speed animation can have.
    /// </summary>
    private float fAnimMinSpeed = 0.5f;

    /// <summary>
    /// Animation normal speed.
    /// </summary>
    private float fAnimNormalSpeed = 1.0f;

    /// <summary>
    /// Max speed change duration in seconds.
    /// </summary>
    private int iSpeedChangeMaxDuration = 5;

    /// <summary>
    /// Min speed change duration in seconds.
    /// </summary>
    private int iSpeedChangeMinDuration = 1;

    /// <summary>
    /// Random value of next speed change duration.
    /// </summary>
    private int iAnimSpeedChangeDuration;

    /// <summary>
    /// If anim speed is changed.
    /// </summary>
    private bool bIsAnimSpeedChanged;
    #endregion

    /// <summary>
    /// Inherited from MonoBehaviour. Use this for initialization.
    /// </summary>
    void Start()
    {
        anim = GetComponent<Animator>();

        // Initializes speed change variables
        animSpeedChange = new System.Diagnostics.Stopwatch();
        iAnimSpeedChangeDuration = 0;
        bIsAnimSpeedChanged = false;

        // Initializes anim type change variables
        animTypeChange = new System.Diagnostics.Stopwatch();
        animTypeChangeInterval = new System.Diagnostics.Stopwatch();
        iAnimTypeChangeDuration = 0;
        iAnimTypeInterval = 0;
        bIsAnimMagic = false;
        anim.SetBool("bIsMagic", bIsAnimMagic);

        UnityEngine.Random.seed = (int)System.DateTime.Now.Ticks;

        // Set first wait time for type change and starts stopwatch
        iAnimTypeInterval = UnityEngine.Random.Range(iTypeMinInterval, iTypeMaxInterval+1);
        animTypeChangeInterval = System.Diagnostics.Stopwatch.StartNew();
    }

    /// <summary>
    /// Inherited from MonoBehaviour. Update is called once per frame.
    /// </summary>
    void Update()
    {
        // Checks if player and enemy are not in battle mode
        if (!CharStatsnSettings.bIsAttackingLocked)
        {
            v2Speed.x = Input.GetAxis("Horizontal");    // Gets axis input to calculate speed/direction value
            v2Speed.y = Input.GetAxis("Vertical");

            v2Direction = v2Speed.normalized;           // Calculates the normalized direction.
            v2Speed.x = Mathf.Abs(v2Speed.x);           // The speed/direction is always positive
            v2Speed.y = Mathf.Abs(v2Speed.y);

            v2Move.x = v2Speed.x * CharStatsnSettings.fSpeed * v2Direction.x * 10000 * Time.deltaTime; // The movement is calculated by multiplying the character speed with the speed according to the correct axis and the correct direction
            v2Move.y = v2Speed.y * CharStatsnSettings.fSpeed * v2Direction.y * 10000 * Time.deltaTime; // Speed is multiplied by 10000 to complensate for necessary linear drag

            // iFacingDirection = GetPlayerFacingDirectionMouse(); // Gets mouse position and calculates facing direction of the player
            if (v2Move != Vector2.zero)
            {
                iFacingDirection = GameObjectExtensions.GetFacingDirectionMovement(v2Move); // Gets player movement and calculates facing direction
            }

            anim.speed = fAnimNormalSpeed;
        }

        if (CharStatsnSettings.bIsAttackingLocked)
        {
            // If battle is locked, stops moving and faces the enemy
            v2Speed = Vector2.zero;

            try
            {
                goEnemy = GameObject.FindGameObjectWithTag("Enemy");
                iFacingDirection = GameObjectExtensions.GetCharaterDirection(this.gameObject, goEnemy);
            }
            catch (Exception e)
            {
                GameManager.gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": Movement2D::GetEnemyDirection - Unable to get enemy gameObject. " + e.ToString());
                Debug.LogError(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": Movement2D::GetEnemyDirection - Unable to get enemy gameObject. " + e.ToString());
            }

            ChangeAnim();
        }

        anim.SetBool("bIsStopped", GameObjectExtensions.CheckIfStopped(v2Speed));            // Set speed parameter for idle/walk animation
        anim.SetInteger("iFacingDirection", (int)iFacingDirection);      // Plays correct animation   
        anim.SetBool("bIsAttacking", CharStatsnSettings.bIsAttackingLocked);
    }

    /// <summary>
    /// Changes and controls character animations.
    /// </summary>
    private void ChangeAnim()
    {
        
        // Speed animation
        // if speed is normal
        if (!bIsAnimSpeedChanged)
        {
            // Randomly gets speed change duration
            iAnimSpeedChangeDuration = UnityEngine.Random.Range(iSpeedChangeMinDuration, iSpeedChangeMaxDuration + 1);

            // Random anim speed 
            anim.speed = UnityEngine.Random.Range(fAnimMinSpeed, fAnimMaxSpeed);

            if (bIsDebugCharacterMovement)
            {
                Debug.Log("iAnimSpeedChangeDuration: " + iAnimSpeedChangeDuration + " | Anim speed set: " + anim.speed);
            }

            // Starts speed change control stopwatch
            animSpeedChange = System.Diagnostics.Stopwatch.StartNew();

            bIsAnimSpeedChanged = true;
        }
        else if (bIsAnimSpeedChanged && animSpeedChange.ElapsedMilliseconds > iAnimSpeedChangeDuration * 1000)
        {
            // If animation is set to different speed and the speed change duration has passed restores states
            anim.speed = fAnimNormalSpeed;
            animSpeedChange.Stop();

            if (bIsDebugCharacterMovement)
            {
                Debug.Log("iAnimSpeedChangeDuration: " + iAnimSpeedChangeDuration + " | Anim speed set: " + anim.speed);
            }
            bIsAnimSpeedChanged = false; 
        }

        // Anim type
        // If magic animation is not active and wait time is ellapsed
        if (!bIsAnimMagic && animTypeChangeInterval.ElapsedMilliseconds > iAnimTypeInterval * 1000)
        {
            // Randomly gets type change duration
            iAnimTypeChangeDuration = UnityEngine.Random.Range(iTypeChangeMinDuration, iTypeChangeMaxDuration + 1);

            if (bIsDebugCharacterMovement)
            {
                Debug.Log("not bIsAnimMagic");
                Debug.Log("iAnimTypeChangeDuration: " + iAnimTypeChangeDuration + " | Anim bIsMagic set: " + anim.GetBool("bIsMagic"));
            }

            // Starts type change control stopwatch
            animTypeChange = System.Diagnostics.Stopwatch.StartNew();

            bIsAnimMagic = true;

            // Sets animation to magic
            anim.SetBool("bIsMagic", bIsAnimMagic);
        }
        else if (bIsAnimMagic && animTypeChange.ElapsedMilliseconds > iAnimTypeChangeDuration * 1000)
        {
            bIsAnimMagic = false;

            // Sets animation to normal attack
            anim.SetBool("bIsMagic", false);
            animTypeChange.Stop();

            animTypeChangeInterval.Stop();
            // Set wait time for type change and starts stopwatch
            iAnimTypeInterval = UnityEngine.Random.Range(iTypeMinInterval, iTypeMaxInterval + 1);
            animTypeChangeInterval = System.Diagnostics.Stopwatch.StartNew();

            if (bIsDebugCharacterMovement)
            {
                Debug.Log("bIsAnimMagic = true");
                Debug.Log("iAnimTypeChangeDuration: " + iAnimTypeChangeDuration + " | Anim bIsMagic set: " + anim.GetBool("bIsMagic"));
            }
        }
    }



    /// <summary>
    /// Inherited from MonoBehaviour. FixedUpdate is called every fixed framerate frame.
    /// Used for physics processing.
    /// </summary>
    void FixedUpdate()
    {
        if (!CharStatsnSettings.bIsAttackingLocked)
        {
            GetComponent<Rigidbody2D>().AddForce(v2Move);
        }
    }
}
