﻿//-----------------------------------------------------------------------
// <copyright file="CharInteraction.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-20, 11:19</last_modified>
// <summary>Defines character interaction with world objects.</summary>
//-----------------------------------------------------------------------
using System.Collections;
using UnityEngine;

/// <summary>
/// Defines character interaction with world objects and facing direction.
/// </summary>
public class CharInteraction : MonoBehaviour
{
    /// <summary>
    /// GameObject to store the object collided and detect if player can move or not.
    /// </summary>
    private GameObject goObjCollided;

    /// <summary>
    /// Show contact dialog only once.
    /// </summary>
    private bool bContactDialogShownOnce = false;

    /// <summary>
    /// Controls that the first victory dialog shows only once.
    /// </summary>
    private bool bFirstVitoryDialogShownOnce = false;

    /// <summary>
    /// Inherited from MonoBehaviour. Update is called once per frame.
    /// </summary>
    void Update()
    {
        // If there is no enemy collision, sets attackinglocked to false so it can move
        if (goObjCollided == null)
        {
            CharStatsnSettings.bIsAttackingLocked = false;
        }

        // If first victory was never shown but first contact was made, means first enemy was defeated
        if (!bFirstVitoryDialogShownOnce && bContactDialogShownOnce && goObjCollided == null)
        {
            MenuUI.ShowSingleDialogByIndex(46);
            bFirstVitoryDialogShownOnce = true;
        }

    }

    /// <summary>
    /// Inherited from MonoBehaviour. OnCollision enter event.
    /// </summary>
    /// /// <param name="c2Collision">Collision that activated the event.</param>
    void OnCollisionEnter2D(Collision2D c2Collision)
    {
        goObjCollided = c2Collision.gameObject;

        if (goObjCollided.CompareTag("Enemy"))
        {
            CharStatsnSettings.bIsAttackingLocked = true;       // to start attack procedure
            goObjCollided.GetComponent<EnemyStatsnSettings>().bCanBeHit = true;     // Enemy collided can be hit since its close
            gameObject.GetComponent<CharMovement2D>().anim.SetBool("bIsAttacking", CharStatsnSettings.bIsAttackingLocked);

            GameManager.Direction iFacingDirection = GameObjectExtensions.GetCharaterDirection(this.gameObject, goObjCollided);
            gameObject.GetComponent<CharMovement2D>().anim.SetInteger("iFacingDirection", (int)iFacingDirection);      // Turns player to enemy

            // Show dialog only the first time it touches an enemy.
            if (!bContactDialogShownOnce)
            {
                MenuUI.AddDialogToList(44, 51, 45);
                bContactDialogShownOnce = true;
            }
        }
    }
}
