﻿//-----------------------------------------------------------------------
// <copyright file="CharStatsnSettings.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-20, 12:12</last_modified>
// <summary>Character base stats and settings.</summary>
//-----------------------------------------------------------------------
using System.Collections;
using UnityEngine;

/// <summary>
/// Defines character base stats and user settings.
/// </summary>
public class CharStatsnSettings : MonoBehaviour
{
    /// <summary>
    /// Player's initial health.
    /// </summary>
    //// public float fHealth = 20;

    /// <summary>
    /// Player's base Attack Power.
    /// </summary>
    public static double dBaseAtkPower = 10;

    /// <summary>
    /// Player's current Attack Power.
    /// </summary>
    public static double dCurrentAtkPower = 10;

    /// <summary>
    /// If player is locked attacking and cannot move.
    /// </summary>
    public static bool bIsAttackingLocked = false;

    /// <summary>
    /// Player's initial Movement speed.
    /// </summary>
    public static float fSpeed = 10;
}
