﻿//-----------------------------------------------------------------------
// <copyright file="GameManager.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-21, 16:14</last_modified>
// <summary>Game master management. Responsible for object creation and logging.</summary>
//-----------------------------------------------------------------------
using System;
using System.IO;
using UnityEngine;

/// <summary>
/// Game management control class.
/// </summary>
public class GameManager : MonoBehaviour
{
    /// <summary>
    /// Log control variable.
    /// </summary>
    private bool bIsInDebugMode = false;

    #region LogFiles Variables
    /// <summary>
    /// Log files location.
    /// </summary>
    public static string sLogDirectory;

    /// <summary>
    /// Bandpowers average log file.
    /// </summary>
    public static string sAverageBandPowersLogFileLocation;

    /// <summary>
    /// Bandpowers raw log file.
    /// </summary>
    public static string sRawBandPowersLogFileLocation;

    /// <summary>
    /// Adjustment perdio log file.
    /// </summary>
    public static string sAdjustmentPeriodLogFileLocation;

    /// <summary>
    /// Game engine log TextWriter.
    /// </summary>
    public static TextWriter gameEngineLog;

    /// <summary>
    /// Average BandPower log TextWriter.
    /// </summary>
    public static TextWriter averageBandPowersLog;

    /// <summary>
    /// Raw BandPower log TextWriter.
    /// </summary>
    public static TextWriter rawBandPowersLog;

    /// <summary>
    /// Adjutsment period log TextWriter.
    /// </summary>
    public static TextWriter adjustmentPeriodLog;

    /// <summary>
    /// Game engine log file.
    /// </summary>
    private static string sGameEngineLogFileLocation;
    #endregion

    /// <summary>
    /// Show welcome dialog only once.
    /// </summary>
    private bool bShownWelcomeOnce = false;

    /// <summary>
    /// Drawn map reference.
    /// </summary>
    private MapDrawer mapdrawer;

    /// <summary>
    /// Enemy spawn reference.
    /// </summary>
    private EnemySpawner enemySpawner;

    /// <summary>
    /// Dynamic Difficulty class reference.
    /// </summary>
    private DynamicDifficulty dynDifficulty;

    /// <summary>
    /// Enum to define facing positions.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// Represents no direction.
        /// </summary>
        None = 0,

        /// <summary>
        /// Represents right direction.
        /// </summary>
        Right = 1,

        /// <summary>
        /// Represents up direction.
        /// </summary>
        Up = 2,

        /// <summary>
        /// Represents left direction.
        /// </summary>
        Left = 3,

        /// <summary>
        /// Represents down direction.
        /// </summary>
        Down = 4
    }

    /// <summary>
    /// Set Log files location.
    /// </summary>
    private static void SetLogFiles()
    {
        // Setting directory below the application folder.
        sLogDirectory = Application.dataPath + "\\Logs";

        // Verifies if directory already exists. If not, creates it.
        if (!Directory.Exists(sLogDirectory))
        {
            Directory.CreateDirectory(sLogDirectory);
        }

        // Gets curretn time to add to log file name
        string sCurrentTime = System.DateTime.Now.ToString("dd-MM-yy_hh.mm.ss");
        // Sets the file names.
        sGameEngineLogFileLocation = sLogDirectory + "\\" + sCurrentTime + "_GameEngineLog.txt";
        sAverageBandPowersLogFileLocation = sLogDirectory + "\\" + sCurrentTime + "_AverageBandPowers.txt";
        sRawBandPowersLogFileLocation = sLogDirectory + "\\" + sCurrentTime + "_RawBandPowers.txt";
        sAdjustmentPeriodLogFileLocation = sLogDirectory + "\\" + sCurrentTime + "_AdjustmentPeriod.txt";

        // Defines the StreamWriter to the corresponding log file.
        gameEngineLog = new StreamWriter(sGameEngineLogFileLocation, false);
        averageBandPowersLog = new StreamWriter(sAverageBandPowersLogFileLocation, false);
        rawBandPowersLog = new StreamWriter(sRawBandPowersLogFileLocation, false);
        adjustmentPeriodLog = new StreamWriter(sAdjustmentPeriodLogFileLocation, false);
    }

    /// <summary>
    /// Close Log files.
    /// </summary>
    public static void CloseLogFiles()
    {
        averageBandPowersLog.Close();
        rawBandPowersLog.Close();
        gameEngineLog.Close();
        adjustmentPeriodLog.Close();
    }

    /// <summary>
    /// Inherited from MonoBehaviour. Use this for initialization.
    /// </summary>
    void Start()
    {
        // Sets log files name and location
        SetLogFiles();

        // Call to "extended" addcomponent function of MapDrawer; sets this as gameobject parent
        mapdrawer = MapDrawer.AddMapDrawerComponent(gameObject);

        // Call to "extended" addcomponent function of EnemySpawner; passes this as parent gameobject and the map drawn
        enemySpawner = EnemySpawner.AddEnemySpawnerComponent(gameObject, mapdrawer);

        // Adds dynamic difficulty management class to object.
        dynDifficulty = gameObject.AddComponent<DynamicDifficulty>();

        // Gets variables ready for execution.
        EngineManager.InitializeHeadsetSettings();
    }

    /// <summary>
    /// Inherited from MonoBehaviour. Update is called once per frame.
    /// </summary>
    void Update()
    {
        if(!bShownWelcomeOnce)
        {
            if(!bIsInDebugMode)
            {
                MenuUI.AddDialogToList(36, 37, 38, 39, 40, 41, 42, 43, 50, 48);
                gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": GameManager::Update() - Going to activate countdown.");
                MenuUI.bGoingToActivateCountdown = true;
            }
            bShownWelcomeOnce = true;
        }

        // Simulation keys
        // If Q is pressed and a thread exists, requests stop
        if (Input.GetKeyDown(KeyCode.Q) && EngineManager.tReadingsThread.IsAlive)
        {
            EngineManager.RequestThreadStop();
        }

        if (EngineManager.bIsEmoComposer)
        {

            // Uptrain alpha management keys
            if (Input.GetKeyDown("[7]"))
            {
                EngineManager.IncreaseUpAlpha();
            }

            if (Input.GetKeyDown("[4]"))
            {
                EngineManager.DecreaseUpAlpha();
            }

            // Downtrain alpha management keys
            if (Input.GetKeyDown("[8]"))
            {
                EngineManager.IncreaseDownAlpha();
            }

            if (Input.GetKeyDown("[5]"))
            {
                EngineManager.DecreaseDownAlpha();
            }

            // Downtrain beta management keys
            if (Input.GetKeyDown("[9]"))
            {
                EngineManager.IncreaseDownBeta();
            }

            if (Input.GetKeyDown("[6]"))
            {
                EngineManager.DecreaseDownBeta();
            }
        }
    }

    /// <summary>
    /// Inherited from MonoBehaviour. Called when gameObject is going to be destroyed.
    /// </summary>
    void OnDestroy()
    {
        // When game is closed checks if thread is active to disconnect the engine. Used when editing-
        if (EngineManager.tReadingsThread != null && EngineManager.tReadingsThread.IsAlive)
        {
            gameEngineLog.WriteLine(DateTime.Now.ToString("dd-MM-yy_hh.mm.ss.ffff") + ": GameManager::OnDestroy() - Going to Request thread stop.");
            EngineManager.RequestThreadStop();
            dynDifficulty.SetFinalInfoForLog();
            CloseLogFiles();
        }
    }
}