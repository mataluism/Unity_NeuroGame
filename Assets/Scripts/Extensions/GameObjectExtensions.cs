﻿//-----------------------------------------------------------------------
// <copyright file="GameObjectExtensions.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-20, 13:05</last_modified>
// <summary>GameObject extension methods. Auxiliary class.</summary>
//-----------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// GameObject extension methods.
/// </summary>
public static class GameObjectExtensions
{
    /// <summary>
    /// Gets the game object sprite bounds.
    /// </summary>
    /// <param name="go">GameObject to make the calculation.</param>
    /// <returns>Array with coordinates [Right; Upper; Left; Lower].</returns>
    public static float[] GetSpriteBounds(this GameObject go)
    {
        float[] fSpriteBounds = new float[4];
        try
        {
            SpriteRenderer sp = go.GetComponent<SpriteRenderer>();
            fSpriteBounds[0] = sp.bounds.extents.x; // Distance to the right, from center point
            fSpriteBounds[1] = sp.bounds.extents.y; // Distance to the upper side
            fSpriteBounds[2] = -sp.bounds.extents.x; // Distance to the left
            fSpriteBounds[3] = -sp.bounds.extents.y; // Distance to the lower side
        }
        catch (Exception e)
        {
            GameManager.gameEngineLog.WriteLine("Unable to get sprite renderer component of game object.GameObjectExtensions.GetSpriteBounds." + e.ToString());
            Debug.LogError("Unable to get sprite renderer component of game object. GameObjectExtensions.GetSpriteBounds." + e.ToString());
        }

        return fSpriteBounds;
    }

    ///// <summary>
    ///// Gets the camera and GameObject bounds and calculates available regions for spawning enemies.
    ///// </summary>
    ///// <param name="go">GameObject to make the calculation.</param>
    ///// <returns>List of SpawnRegions.</returns>
    //public static List<SpawnRegion> CalculateAvailableRegions(this GameObject go)
    //{
    //    List<SpawnRegion> lRegions = new List<SpawnRegion>();   // Coordinates of regions [Right,Upper,Left,Bottom][x,y,width,height]

    //    float[] fCamBounds = new float[4];              // Gets the camera bounds and store them in an array
    //    fCamBounds = CameraExtensions.CameraBounds(Camera.main);

    //    //// GameObject[] floorGO;				// Gets all GameObjects with tag floor
    //    //// floorGO = GameObject.FindGameObjectsWithTag ("Floor");

    //    float[] fSpriteBounds = new float[4];   // Gets dimensions of first GameObject
    //    fSpriteBounds = GetSpriteBounds(go);

    //    // UPPER REGION
    //    // Check if there is a region available on top
    //    // If Upper sprite bound is higher than upper cam bound (if the subtraction is positive), means that there is space for the spawn
    //    if (fSpriteBounds[1] - fCamBounds[1] > 0)
    //    {
    //        SpawnRegion temporaryRegion = new SpawnRegion();
    //        temporaryRegion.SetCardinalPoint(GameManager.Direction.Up);  // Which region is this
    //        temporaryRegion.SetPosition(fSpriteBounds[2], fCamBounds[1]);   // x position = floor limit ; y position = camera top limit
    //        temporaryRegion.SetWidth(fSpriteBounds[0] - fSpriteBounds[2]);  // width = spriteRight - spriteLeft
    //        temporaryRegion.SetHeight(fSpriteBounds[1] - fCamBounds[1]);        // height = spriteUp - camUp

    //        // Gets area to calculate random probability; adds to the list a number of times according to the area (means bigger areas are mor probable to being choosen
    //        float area = temporaryRegion.GetRegionArea();

    //        // Debug.Log("area: " + area);
    //        int probabilityRounded = (int)Math.Round(area, 0);
    //        Debug.Log("num added prob: " + probabilityRounded);
    //        for (int i = 0; i < probabilityRounded; i++)
    //        {
    //            lRegions.Add(temporaryRegion);
    //        }

    //        // Debug.Log("Added Up region" + lRegions.Count);
    //    }

    //    // LOWER REGION
    //    // Check if there is a region available on bottom
    //    // If Lower cam bound is lower than low sprite bound (if the subtraction is positive), means that there is space for the spawn
    //    if (fCamBounds[3] - fSpriteBounds[3] > 0)
    //    {
    //        SpawnRegion temporaryRegion = new SpawnRegion();
    //        temporaryRegion.SetCardinalPoint(GameManager.Direction.Down);    // Which region is this
    //        temporaryRegion.SetPosition(fSpriteBounds[2], fSpriteBounds[3]);    // x position = floor limit ; y position = floor limit
    //        temporaryRegion.SetWidth(fSpriteBounds[0] - fSpriteBounds[2]);  // width = spriteRight - spriteLeft
    //        temporaryRegion.SetHeight(Mathf.Abs(fSpriteBounds[3] - fCamBounds[3]));     // height = spriteDown - camDown

    //        // Gets area to calculate random probability; adds to the list a number of times according to the area (means bigger areas are mor probable to being choosen
    //        float area = temporaryRegion.GetRegionArea();
    //        Debug.Log("area: " + area);
    //        int probabilityRounded = (int)Math.Round(area, 0);
    //        Debug.Log("num added prob: " + probabilityRounded);
    //        for (int i = 0; i < probabilityRounded; i++)
    //        {
    //            lRegions.Add(temporaryRegion);
    //        }

    //        // Debug.Log("Added Lower region" + lRegions.Count);
    //    }

    //    // RIGHT REGION
    //    // Check if there is a region available on the right
    //    // If Right sprite bound is "righter" than R cam bound (if the subtraction is positive), means that there is space for the spawn
    //    if (fSpriteBounds[0] - fCamBounds[0] > 0)
    //    {
    //        SpawnRegion temporaryRegion = new SpawnRegion();
    //        temporaryRegion.SetCardinalPoint(GameManager.Direction.Right);   // Which region is this

    //        // If sentence to avoid region overlap/spawn outside map
    //        // If spriteDown is lower than camDown && if spriteTop is upper than camTop = most common situation -> must be used cam Y limits
    //        if (fSpriteBounds[3] < fCamBounds[3] && fSpriteBounds[1] > fCamBounds[1])
    //        {
    //            temporaryRegion.SetPosition(fCamBounds[0], fCamBounds[3]);      // x position = cam right limit ; y position = camera low limit
    //            temporaryRegion.SetHeight(fCamBounds[1] - fCamBounds[3]);       // height = camTop - camDown
    //        }
    //        else if (fSpriteBounds[3] > fCamBounds[3] && fSpriteBounds[1] < fCamBounds[1])
    //        {
    //            // If spriteDown is upper than camDown &&  if spriteTop is lower than camTop = must be used sprite Y limits
    //            temporaryRegion.SetPosition(fCamBounds[0], fSpriteBounds[3]);       // x position = cam right limit ; y position = sprite low limit
    //            temporaryRegion.SetHeight(fSpriteBounds[1] - fSpriteBounds[3]);     // height = spriteTop - spriteDown
    //        }
    //        else if (fSpriteBounds[3] < fCamBounds[3])
    //        {
    //            // If spriteDown is lower than camDown (and none of the previous if sentences) = camDown limit considered
    //            temporaryRegion.SetPosition(fCamBounds[0], fCamBounds[3]);      // x position = cam right limit ; y position = cam low limit
    //            temporaryRegion.SetHeight(fSpriteBounds[1] - fCamBounds[3]);        // height = spriteTop - camDown
    //        }
    //        else if (fSpriteBounds[1] > fCamBounds[1])
    //        {
    //            // If spriteUp is upper than camUp (and none of the previous if sentences) = camUp limit considered
    //            temporaryRegion.SetPosition(fCamBounds[0], fSpriteBounds[3]);       // x position = cam right limit ; y position = sprite low limit
    //            temporaryRegion.SetHeight(fCamBounds[1] - fSpriteBounds[3]);        // height = camTop - spriteDown
    //        }

    //        temporaryRegion.SetWidth(fSpriteBounds[0] - fCamBounds[0]); // width = spriteRight - camRight

    //        // Gets area to calculate random probability; adds to the list a number of times according to the area (means bigger areas are mor probable to being choosen
    //        float area = temporaryRegion.GetRegionArea();
    //        Debug.Log("area: " + area);
    //        int probabilityRounded = (int)Math.Round(area, 0);
    //        Debug.Log("num added prob: " + probabilityRounded);
    //        for (int i = 0; i < probabilityRounded; i++)
    //        {
    //            lRegions.Add(temporaryRegion);
    //        }

    //        // Debug.Log("Added Right region" + lRegions.Count);
    //    }

    //    // LEFT REGION
    //    // Check if there is a region available on the left
    //    // If Left cam bound is "lefter" than left sprite bound (if the subtraction is positive), means that there is space for the spawn
    //    if (fCamBounds[2] - fSpriteBounds[2] > 0)
    //    {
    //        SpawnRegion temporaryRegion = new SpawnRegion();
    //        temporaryRegion.SetCardinalPoint(GameManager.Direction.Left);    // Which region is this

    //        // If sentence to avoid region overlap/spawn outside map
    //        // If spriteDown is lower than camDown && if spriteTop is upper than camTop = most common situation -> must be used cam Y limits
    //        if (fSpriteBounds[3] < fCamBounds[3] && fSpriteBounds[1] > fCamBounds[1])
    //        {
    //            temporaryRegion.SetPosition(fSpriteBounds[2], fCamBounds[3]);       // x position = cam right limit ; y position = camera low limit
    //            temporaryRegion.SetHeight(fCamBounds[1] - fCamBounds[3]);       // height = camTop - camDown
    //        }
    //        else if (fSpriteBounds[3] > fCamBounds[3] && fSpriteBounds[1] < fCamBounds[1])
    //        {
    //            // If spriteDown is upper than camDown &&  if spriteTop is lower than camTop = must be used sprite Y limits
    //            temporaryRegion.SetPosition(fSpriteBounds[2], fSpriteBounds[3]);        // x position = cam right limit ; y position = sprite low limit
    //            temporaryRegion.SetHeight(fSpriteBounds[1] - fSpriteBounds[3]);     // height = spriteTop - spriteDown
    //        }
    //        else if (fSpriteBounds[3] < fCamBounds[3])
    //        {
    //            // If spriteDown is lower than camDown (and none of the previous if sentences) = camDown limit considered
    //            temporaryRegion.SetPosition(fSpriteBounds[2], fCamBounds[3]);       // x position = cam right limit ; y position = cam low limit
    //            temporaryRegion.SetHeight(fSpriteBounds[1] - fCamBounds[3]);        // height = spriteTop - camDown
    //        }
    //        else if (fSpriteBounds[1] > fCamBounds[1])
    //        {
    //            // If spriteUp is upper than camUp (and none of the previous if sentences) = camUp limit considered
    //            temporaryRegion.SetPosition(fSpriteBounds[2], fSpriteBounds[3]);        // x position = cam right limit ; y position = sprite low limit
    //            temporaryRegion.SetHeight(fCamBounds[1] - fSpriteBounds[3]);        // height = camTop - spriteDown
    //        }

    //        temporaryRegion.SetWidth(fCamBounds[2] - fSpriteBounds[2]); // width = spriteRight - camRight

    //        // Gets area to calculate random probability; adds to the list a number of times according to the area (means bigger areas are mor probable to being choosen
    //        float area = temporaryRegion.GetRegionArea();
    //        Debug.Log("area: " + area);
    //        int probabilityRounded = (int)Math.Round(area, 0);
    //        Debug.Log("num added prob: " + probabilityRounded);
    //        for (int i = 0; i < probabilityRounded; i++)
    //        {
    //            lRegions.Add(temporaryRegion);
    //        }

    //        // Debug.Log("Added Left region"+ lRegions.Count);
    //    }

    //    // Debug.Log("NumRegions: " + numRegions + " x: " + lRegions[1].position.x + " y: " + lRegions[1].position.y + " width: " + lRegions[1].width + " height: " + lRegions[1].height);
    //    // Debug.Log("NumRegions: " + numRegions + " x: " + lRegions[3].position.x + " y: " + lRegions[3].position.y + " width: " + lRegions[3].width + " height: " + lRegions[3].height);
    //    // Debug.Log("NumRegions: " + numRegions + " x: " + lRegions[0].position.x + " y: " + lRegions[0].position.y + " width: " + lRegions[0].width + " height: " + lRegions[0].height);
    //    // Debug.Log("NumRegions: " + regions.Count + " x: " + lRegions[0].GetPosition().x + " y: " + lRegions[0].GetPosition().y + " width: " + lRegions[0].GetWidth() + " height: " + lRegions[0].GetHeight());
    //    return lRegions;
    //}

    /// <summary>
    /// Gets destination gameobject position relative to the origin position and sets
    /// iFacingDirection parameter to control the animations correctly.
    /// </summary>
    /// <param name="goOriginObject">The orginin gameObject to calculate the facing direction.</param>
    /// <param name="goDestinObject">The destination gameObject to which the origin GO will be facing.</param>
    /// <returns>Enemy direction.</returns>
    public static GameManager.Direction GetCharaterDirection(GameObject goOriginObject, GameObject goDestinObject)
    {
        GameManager.Direction iFacingDirection = 0;

        // Get GameObjects positions for calculation
        Vector2 v2OriginObjectPos = goOriginObject.transform.position;
        Vector2 v2DestinObjectPos = goDestinObject.transform.position;

        // The difference between enemy and player to know if he's facing right or left
        float fDifferenceX = v2DestinObjectPos.x - v2OriginObjectPos.x;
        float fDifferenceY = v2DestinObjectPos.y - v2OriginObjectPos.y;

        // If  differenceX is bigger than differenceY the Destin is more to the left/right than up/down
        if (Mathf.Abs(fDifferenceX) > Mathf.Abs(fDifferenceY))
        {
            if (fDifferenceX > 0)
            {
                // Origin facing right
                iFacingDirection = GameManager.Direction.Right;  // Sets variable to correct animation
            }

            if (fDifferenceX < 0)
            {
                // Origin facing left
                iFacingDirection = GameManager.Direction.Left;   // Sets variable to correct animation
            }
        }

        if (Mathf.Abs(fDifferenceX) < Mathf.Abs(fDifferenceY))
        {   // Means Origin is either facing up or down
            if (fDifferenceY > 0)
            {   // Origin facing up
                iFacingDirection = GameManager.Direction.Up; // Sets variable to correct animation
            }

            if (fDifferenceY < 0)
            {   // Origin facing down
                iFacingDirection = GameManager.Direction.Down;   // Sets variable to correct animation
            }
        }

        return iFacingDirection;
    }

    /// <summary>
    /// Gets the game object movement and sets iFacingDirection parameter to control the animations correctly.
    /// </summary>
    /// <param name="v2MoveSpeed">Current GameObject movement speed.</param>
    /// <returns>The facing direction.</returns>
    public static GameManager.Direction GetFacingDirectionMovement(Vector2 v2MoveSpeed)
    {
        GameManager.Direction iFacingDirection = 0;

        // If x bigger, he is facing left/right
        if (Mathf.Abs(v2MoveSpeed.x) > Mathf.Abs(v2MoveSpeed.y))
        {
            if (v2MoveSpeed.x > 0)
            {
                // Player facing right
                iFacingDirection = GameManager.Direction.Right;  // Sets variable to correct animation
            }

            if (v2MoveSpeed.x < 0)
            {
                // Player facing left
                iFacingDirection = GameManager.Direction.Left;   // Sets variable to correct animation
            }
        }

        if (Mathf.Abs(v2MoveSpeed.x) < Mathf.Abs(v2MoveSpeed.y))
        {   // Means player is either facing up or down
            if (v2MoveSpeed.y > 0)
            {   // Player facing up
                iFacingDirection = GameManager.Direction.Up; // Sets variable to correct animation
            }

            if (v2MoveSpeed.y < 0)
            {   // Player facing down
                iFacingDirection = GameManager.Direction.Down;   // Sets variable to correct animation
            }
        }

        return iFacingDirection;
    }

    /// <summary>
    /// Checks if gameobject has stopped movement (if speed lower than 0.1f).
    /// </summary>
    /// <param name="currentSpeed">Current speed vector2 to calculate.</param>
    /// <returns>If is stopped or not.</returns>
    public static bool CheckIfStopped(Vector2 currentSpeed)
    {
        if (Mathf.Abs(currentSpeed.x) < 0.1f && Mathf.Abs(currentSpeed.y) < 0.1f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}