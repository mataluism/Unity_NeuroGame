﻿//-----------------------------------------------------------------------
// <copyright file="SmoothCamera2D.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-21, 16:30</last_modified>
// <summary>Smoothes camera movement when following player.</summary>
//-----------------------------------------------------------------------
using System.Collections;
using UnityEngine;

/// <summary>
/// Camera smooth movement.
/// </summary>
public class SmoothCamera2D : MonoBehaviour
{
    /// <summary>
    /// Smooth movement time.
    /// </summary>
    public float fDampTime = 0.15f;

    /// <summary>
    /// Camera velocity.
    /// </summary>
	private Vector3 v3Velocity = Vector3.zero;

    /// <summary>
    /// Target transform to follow.
    /// </summary>
	private Transform tTarget;

    /// <summary>
    /// Inherited from MonoBehaviour. Use this for initialization.
    /// </summary>
	void Start()
    {
        tTarget = GameObject.FindWithTag("Player").transform;   // Gets player target automatically
    }

    /// <summary>
    /// Inherited from MonoBehaviour. Update is called once per frame.
    /// </summary>
    void Update()
    {
        if (tTarget)
        {
            Vector3 point = GetComponent<Camera>().WorldToViewportPoint(tTarget.position);
            Vector3 delta = tTarget.position - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); // (new Vector3(0.5, 0.5, point.z));
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref v3Velocity, fDampTime);
        }
    }
}
