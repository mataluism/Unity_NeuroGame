﻿//-----------------------------------------------------------------------
// <copyright file="CameraExtensions.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-20, 13:02</last_modified>
// <summary>Camera extension methods. Auxiliary class.</summary>
//-----------------------------------------------------------------------
using System.Collections;
using UnityEngine;

/// <summary>
/// Camera extension methods.
/// </summary>
public static class CameraExtensions
{
    /// <summary>
    /// Gets the lower camera bound in world coordinates.
    /// </summary>
    /// <param name="cam">Camera to make the calculation.</param>
    /// <returns>Lower Camera bound in world coordinates.</returns>
    public static float LowerCameraBound(this Camera cam)
    {
        return cam.transform.position.y - cam.orthographicSize;
    }

    /// <summary>
    /// Gets the upper camera bound in world coordinates.
    /// </summary>
    /// <param name="cam">Camera to make the calculation.</param>
    /// <returns>Upper Camera bound in world coordinates.</returns>
    public static float UpperCameraBound(this Camera cam)
    {
        return cam.transform.position.y + cam.orthographicSize;
    }

    /// <summary>
    /// Gets the left camera bound in world coordinates.
    /// </summary>
    /// <param name="cam">Camera to make the calculation.</param>
    /// <returns>Left Camera bound in world coordinates.</returns>
    public static float LeftCameraBound(this Camera cam)
    {
        return cam.transform.position.x - (cam.orthographicSize * Screen.width / Screen.height);
    }

    /// <summary>
    /// Gets the right camera bound in world coordinates.
    /// </summary>
    /// <param name="cam">Camera to make the calculation.</param>
    /// <returns>Right Camera bound in world coordinates.</returns>
    public static float RightCameraBound(this Camera cam)
    {
        return cam.transform.position.x + (cam.orthographicSize * Screen.width / Screen.height);
    }

    /// <summary>
    /// Gets all the camera bounds in world coordinates.
    /// </summary>
    /// <param name="cam">Camera to make the calculation.</param>
    /// <returns>Array with coordinates [Right; Upper; Left; Lower].</returns>
    public static float[] CameraBounds(this Camera cam)
    {
        float[] myArray = new float[4];
        myArray[0] = CameraExtensions.RightCameraBound(cam);
        myArray[1] = CameraExtensions.UpperCameraBound(cam);
        myArray[2] = CameraExtensions.LeftCameraBound(cam);
        myArray[3] = CameraExtensions.LowerCameraBound(cam);

        return myArray;
    }
}
