﻿//-----------------------------------------------------------------------
// <copyright file="DebuggingTools.cs" company="Luís Mata">
//		Copyright (c) 2016 Luís Mata.
// </copyright>
// <project>TheDream2D.CSharp developed solely for academic purposes.</project>
// <author>Luís Mata</author>
// <last_modified>2016-8-20, 13:05</last_modified>
// <summary>Auxiliary class with all around debugging auxiliary tools.</summary>
//-----------------------------------------------------------------------
using System.Collections;
using System.IO;
using UnityEngine;

/// <summary>
/// GameObject extension methods.
/// </summary>
public class DebuggingTools
{
    /// <summary>
    /// Prints matrix. Used for debug.
    /// </summary>
    /// <param name="array">Matrix to print.</param>
    public static void PrintArrayMatrix(int[,] array)
    {
        int lengx = array.GetLength(0);
        int lengy = array.GetLength(1);

        string arr = string.Empty;

        // Debug.Log("Array size: " + lengx + " " + lengy);
        for (int x = 0; x < lengx; x++)
        {
            for (int y = 0; y < lengy; y++)
            {
                arr += array[x, y].ToString(" 0 ");
            }

            arr += "\n";
        }

        Debug.Log(arr);
    }

    /// <summary>
    /// Auxiliary function to print current array values.
    /// </summary>
    /// <param name="dArray">Array to print to log.</param>
    public static void PrintArray(double[] dArray)
    {
        for (int i = 0; i < dArray.Length; i++)
        {
            Debug.Log("array pos : " + i + " value: " + dArray[i]);
            // GameManager.adjustmentPeriodLog.WriteLine("array pos : " + i + " value: " + dArray[i]);
        }
    }

    /// <summary>
    /// Auxiliary function to print current array values into log file.
    /// </summary>
    /// <param name="dArray">Array to print to log.</param>
    public static void PrintArrayToLogFile(double[] dArray, TextWriter logFile)
    {
        for (int i = 0; i < dArray.Length; i++)
        {
            // Debug.Log("array pos : " + i + " value: " + dArray[i]);
            logFile.WriteLine("array pos : " + i + " value: " + dArray[i]);
        }
    }
}
